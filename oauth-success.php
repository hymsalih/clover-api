<?php
/**
 * Created by PhpStorm.
 * User: Salih Mohamed
 * Date: 8/4/2019
 * Time: 11:38 AM
 */
session_start();
if (!isset($_SESSION['user'])) {
//    header("Location: login.php");
//    exit;
}
$access_token = (isset($_SESSION['access_token']) ? $_SESSION['access_token'] : '');
if (empty($access_token)) {
    header("Location: login.php");
    exit;
}
include __DIR__ . "/db/Database.php";
//$userId = $_SESSION['user']['id'];
$merchant_id = $_SESSION['merchant_id'];
$userId = 1;
$db = new Database();
$db->executeQuery("INSERT INTO `tbl_clover_authentications` (`id`, `user_id`,`merchant_id`, `token`, `created_at`) VALUES (NULL, '" . $userId . "',  '" . $merchant_id . "', '" . $access_token . "', CURRENT_TIMESTAMP);");
?>
<!doctype html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="title" content="Best eCommerce platform for multi-store retailers. Free trial."/>
    <meta name="robots" content="index, follow"/>
    <meta name="description"
          content="Online stores that help merchants grow sales and operations across online and retail channels."/>
    <meta name="keywords"
          content="e commerce,eCommerce,web store,online retail,merchant,inventory,cost of goods sold,shipping,order processing,bundles,pickup in-store, fulfillment"/>
    <meta name="language" content="en"/>
    <title>Best eCommerce platform for multi - Clover</title>
    <link rel="shortcut icon" href="/favicon.ico"/>
    <link href='https://fonts.googleapis.com/css?family=Cabin:400,600|Open+Sans:300,600,400' rel='stylesheet'>
    <link rel="stylesheet" type="text/css" media="screen" href="https://w3bstore.com/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" media="screen" href="https://w3bstore.com/css/animate.min.css"/>
    <link rel="stylesheet" type="text/css" media="screen" href="https://w3bstore.com/css/icons.css"/>
    <link rel="stylesheet" type="text/css" media="screen" href="https://w3bstore.com/css/landing.css"/>
    <link rel="stylesheet" type="text/css" media="screen" href="https://w3bstore.com/css/responsive.css"/>
    <link rel="stylesheet" type="text/css" media="screen" href="https://w3bstore.com/css/blue.css"/>
    <script src="https://w3bstore.com//js/jquery-3.3.1.min.js"></script>
    <style>
        .ulogo {
            position: relative;
            top: -12px;
            width: 150px;
            height: 40px
        }

        .sticky-navigation .navbar-brand img {
            max-width: 150px !important;
        }

        @media screen and (min-width: 340px) and (max-width: 600px) {
            .ulogo {
                top: 0px;
                width: 150px;
                height: 25px
            }

            .sticky-navigation .navbar-brand img {
                max-width: 150px !important;
            }
        }

        @media screen and (max-width: 320px) {
            .ulogo {
                top: 0px;
                width: 150px;
                height: 25px
            }

            .sticky-navigation .navbar-brand img {
                max-width: 150px !important;
            }
        }

        .clover-logo {
            width: 165px;
            height: 40px;
            margin-top: 0;
            background-image: url(https://files.readme.io/734e66d-clover-logo.svg);
        }

        .header {
            background: url("https://w3bstore.com/images/background-1.jpg") no-repeat fixed center top / cover rgba(0, 0, 0, 0);
        }

        .features {
            background: url(https://w3bstore.com/images/background-1.jpg) no-repeat top center fixed;
        }

        .stats {
            background: url(https://w3bstore.com/images/background-2.jpg) no-repeat center top fixed;
        }

        .call-to-action {
            background: url(https://w3bstore.com/images/background-2.jpg) fixed no-repeat top center;
        }
    </style>
</head>
<body data-spy="scroll" data-offset="170" data-target="#my-nav">
<div class="navbar navbar-inverse bs-docs-nav navbar-fixed-top sticky-navigation" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#stamp-navigation">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-menu"></span>
            </button>
            <a class="navbar-brand" href="#" style="margin-top: 5px;padding: 10px;">
                <img width="150" class="ulogo" src="https://w3bstore.com/images/logo_w3bstore_transparent.png" alt="">
            </a>
        </div>
    </div>
</div>
<section class="brief white-bg-border" id="section2">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <center>
                    <img style="width: 25%; margin-top: 50px" src="https://files.readme.io/734e66d-clover-logo.svg">
                </center>
                <h3 class="text-center dark-text">Grant Application Access to <strong
                            style="font-weight: bolder; color: #085394">Clover</strong></h3>
                <div class="colored-line"></div>
                <p>Great! Your successfully authenticated to <b>W3bstore</b> account with <b> Clover</b>.
                </p>
                <p>
                <div class="alert alert-success" role="alert">
                    <?php echo $access_token; ?>
                </div>
                </p>
                <p class="text-center">
                    <a href="logout.php" class="btn btn-primary standard-button">LOGOUT</a>
                </p>
                <p class="text-center">
                    <span>Take me to the <a href="#">W3bstore homepage</a></span>
                </p>
            </div>
        </div>
    </div>
</section>
<footer class="footer grey-bg" style="clear:both;">
    ©2018 w3bstore.com LLC. All Rights Reserved
    <ul class="footer-links small-text">
        <li><a href="javascript:void(null);"
               onClick="popupWin = window.open('http://w3bstore.com/market.php/default/about', 'contacts', 'location,width=700,height=700,top=200,left=200,scrollbars=1,toolbar=no,location=no,status=no'); popupWin.focus(); return false;"
               class="dark-text">About Us</a>
        </li>
        <li><a href="javascript:void(null);"
               onClick="popupWin = window.open('http://w3bstore.com/market.php/default/privacy', 'contacts', 'location,width=700,height=700,top=200,left=200,scrollbars=1,toolbar=no,location=no,status=no'); popupWin.focus(); return false;"
               class="dark-text">Privacy</a>
        </li>
        <li><a href="javascript:void(null);"
               onClick="popupWin = window.open('http://w3bstore.com/market.php/default/terms', 'contacts', 'location,width=700,height=700,top=200,left=200,scrollbars=1,toolbar=no,location=no,status=no'); popupWin.focus(); return false;"
               class="dark-text">Terms</a>
        </li>
    </ul>
</footer>
<script src="https://w3bstore.com/js/smoothscroll.js"></script>
<script src="https://w3bstore.com/js/bootstrap.minnew.js"></script>
<script src="https://w3bstore.com/js/jquery.nav.js"></script>
<script src="https://w3bstore.com/js/wow.min.js"></script>
<script src="https://w3bstore.com/js/nivo-lightbox.min.js"></script>
<script src="https://w3bstore.com/js/owl.carousel.min.js"></script>
<script src="https://w3bstore.com/js/jquery.stellar.min.js"></script>
<script src="https://w3bstore.com/js/retina.min.js"></script>
<script src="https://w3bstore.com/js/jquery.ajaxchimp.min.js"></script>
<script src="https://w3bstore.com/js/custom.js"></script>
</body>
</html>
