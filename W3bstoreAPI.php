<?php
/**
 * Created by PhpStorm.
 * User: Salih Mohamed
 * Date: 1/6/2020
 * Time: 3:56 PM
 */

namespace W3bStore;
class W3bstoreAPI
{
    private $storeId;
    private $merchantId;
    private $apiKey;
    private $callName;

    public function __construct($storeId, $merchantId, $apiKey)
    {
        $this->storeId = $storeId;
        $this->merchantId = $merchantId;
        $this->apiKey = $apiKey;
        $this->callName = "";
    }

    public function request($url, $method, $requestBody = null, $callName, $parameters = null)
    {
        $this->callName = $callName;
        $authParams = "id=" . $this->storeId . "&merchant_id=" . $this->merchantId . "&api_key=" . $this->apiKey . (!empty($parameters) ? "&" . http_build_query($parameters) : '');
        $curl = curl_init();
        $requestCurl = array(
            CURLOPT_URL => $url . "/api/" . $this->callName . "?" . $authParams,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_CUSTOMREQUEST => $method,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
            ),
        );
        if ($method == 'POST') {
            $requestCurl[CURLOPT_POSTFIELDS] = $requestBody;
        }
        curl_setopt_array($curl, $requestCurl);
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            return ['error' => 1, 'msg' => $err, 'response' => null];
        } else {
            if ($this->callName == 'add_items' || $this->callName == 'add_customers') {
                $responseArray = json_decode($response, true);
                if (!empty($responseArray['error'])) {
                    return ['error' => 1, 'msg' => $responseArray['error'], 'response' => null];
                }
                $msg = "";
                if (!empty($responseArray) && $responseArray['result'] == 'ERROR') {
                    foreach ($responseArray['error_msg'] as $erMsg) {
                        $msg .= $erMsg . "\n";
                    }
                    return ['error' => 1, 'msg' => $msg, 'response' => null];
                }
            }
            $validateResponse = json_decode($response, true);
            if (isset($validateResponse) && isset($validateResponse['error'])) {
                return ['error' => 1, 'msg' => $validateResponse['error']];
            }
            return ['error' => 0, 'msg' => null, 'response' => json_decode($response, true)];
        }
    }

    function logApiError($auth_id, $call_name, $request, $response, $msg)
    {
        global $db;
        $sql = "INSERT INTO `tbl_clover_api_log` (`id`, `auth_id`, `call_name`, `request_payload`, `response_payload`, `msg`) VALUES(NULL, '$auth_id','$call_name', '" . json_encode($request) . "', '" . json_encode($response) . "', '" . addslashes($msg) . "')";
        $db->executeQuery($sql);
    }


}