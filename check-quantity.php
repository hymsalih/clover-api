<?php
/**
 * Created by PhpStorm.
 * User: Salih Mohamed
 * Date: 3/28/2020
 * Time: 3:48 PM
 */
set_time_limit(0);
include __DIR__ . "/db/Database.php";
$db = new Database();
include './CloverAPI.php';
include './W3bstoreAPI.php';
include './vendor/autoload.php';
require_once(__DIR__ . '/vendor/autoload.php');
if (file_exists('./env.php')) {
    include './env.php';
}
$userId = "3402560";
$locationId = "974";
$storeId = "54061";
$currentStock = 0;
$cloverProductId = 'S02N728YG9ZNC';
$merchants = $db->fetchResult("SELECT * from tbl_clover_authentications WHERE location_id ='$locationId' AND store_id ='$storeId' AND user_id = '$userId' AND merchant_id IS NOT NULL AND w3b_merchant_id IS NOT NULL AND w3b_api_key IS NOT NULL AND token IS NOT NULL");
if (empty($merchants)) {
    echo json_encode(['error' => 1, 'msg' => 'Merchant not found']);
    exit;
}
$merchant = $merchants[0];
if ($cloverProductId) {
    $CloverAPI = new W3bStore\CloverAPI();
    $response = $CloverAPI->getProduct($cloverProductId, ['itemStock']);
    if (!empty($response->id)) {
        $cloverProductId = $db->fetchResult("SELECT * FROM tbl_clover_products WHERE clover_id = '$cloverProductId'");
        if (empty($cloverProductId))
            echo json_encode(['error' => 1, 'msg' => 'Clover product not found in w3bstore']);
        $cloverProductId = $cloverProductId[0]['id'];
        $stock = $response->itemStock->quantity;
        if ($stock != $currentStock) {
            $w3bstore = new W3bStore\W3bstoreAPI($storeId, $merchant['w3b_merchant_id'], $merchant['w3b_api_key'], 'setqty/');
            $response = $w3bstore->request('POST', json_encode(['item_detail_id' => $cloverProductId, 'qty' => $stock]));
            if ($response['error'] == 1) {
                echo json_encode(['error' => 1, 'msg' => $response['msg']]);
            } else {
                $syncPrducts = $response['response'];
                $syncPrduct = $syncPrducts[0];
                if ($syncPrduct['result'] == 'OK' || $syncPrduct['result'] == 'ok') {
                    $db->executeQuery("UPDATE tbl_clover_products_stock SET SET stock_count=$stock, quantity=$stock WHERE id = '" . $syncPrduct['stock_id'] . "'");
                    echo json_encode(['error' => 0, 'msg' => 'Stock successfully updated']);
                } else {
                    $w3bstore->logApiError($merchant['store_id'], $syncPrduct['item_detail_id'], null, null, $status = 0, 'update-w3bstore-qty');
                    echo json_encode(['error' => 1, 'msg' => 'Stock not updated successfully']);
                }
            }
        }
    }
}
exit;
