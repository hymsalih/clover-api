<?php
$curl = curl_init();
$requestCurl = array(
    CURLOPT_URL => $url,
    CURLOPT_SSL_VERIFYHOST => false,
    CURLOPT_SSL_VERIFYPEER => false,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_CUSTOMREQUEST => 'GET',
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_HTTPHEADER => array(
        "Content-Type: application/json",
    ),
);
curl_setopt_array($curl, $requestCurl);
$response = curl_exec($curl);
$err = curl_error($curl);
curl_close($curl);
if ($err) {
    print_r($err);
} else {
    print_r($response);
}
