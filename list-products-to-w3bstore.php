<?php
/**
 * Created by PhpStorm.
 * User: Salih Mohamed
 * Date: 12/3/2019
 * Time: 9:56 PM
 */

set_time_limit(0);
include __DIR__ . "/db/Database.php";
include __DIR__ . '/CloverAPI.php';
include __DIR__ . '/W3bstoreAPI.php';
include __DIR__ . '/vendor/autoload.php';

$db = new Database();
if (file_exists('./env.php')) {
    include './env.php';
}
$sql = "SELECT * FROM tbl_clover_products WHERE in_clover=1 AND product_ref IS NOT NULL AND in_w3bstore =0 group by product_ref order by id desc LIMIT 1";
//$sql = "select * from tbl_clover_products where in_clover= 1 and in_w3bstore = 0 and item_group_id = 52  group by item_group_id order by id desc ";
$products = $db->fetchResult($sql);
$merchants = [];
foreach ($products as $product) {
    $modifiersArray = [];
    $sql = "SELECT * FROM `tbl_clover_modifier_group` WHERE ref_id='" . $product['modifier_group_id'] . "'";
    $modifierGroups = $db->fetchResult($sql);
    $modiferLists = [];
    if (!empty($modifierGroups)) {
        foreach ($modifierGroups as $modifierGroup) {
            $sql = "SELECT * FROM tbl_clover_modifiers WHERE modifier_group_id = '" . $modifierGroup['id'] . "'";
            $modifiers = $db->fetchResult($sql);
            $modifiersVariations = [];
            foreach ($modifiers as $modifier) {
                $modifiersVariations[] = [
                    'name' => $modifier['title'],
                    'location_name' => "Main store",
                    "weight" => "",
                    "height" => "",
                    "length" => "",
                    "width" => "",
                    "barcode" => $product['code'],
                    "reorder_point" => "",
                    "item_details" => [
                            "lot_remaining" => (!empty($stock) ? $stock[0]['stock_count'] : 0),
                            "cogs_per_unit" => "",
                        "offer_price" => ($modifier['price']/100),
                        "commission" => "",
//                        "wholesale_price" => $modifier['price'],
                            "unit_cost" => ($product['cost'] == 0 ? "" : $product['cost']),
//                            "list_price" => ($productList['price'] / 100),
                            "warehouse_bin" => "",
                            "low_threshold" => "",
                    ],
                ];
            }
            $modiferLists[] = [
                "Legacy SKU" => "",
                "Product Title" => $modifierGroup['title'],
                "Product Category" => [
                    [
                        "name" => "",
                    ]
                ],
                "Variants" => $modifiersVariations,
                "Tax - exempt" => "",
                "Make" => "",
                "Model" => "",
                "Summary Description" => "",
                "Detailed Description" => "",
                "Ship to customer" => "",
                "Pickup in store" => "",
                "Charge by Weight" => "",
                "Accessories Title" => "",
                "Accessories Limit" => "",
                "Accessories Position" => "",
            ];
        }
    }
    $variations = [];
    $attributeId = [];
    $sql = "SELECT * FROM tbl_clover_products WHERE in_clover=1 AND in_w3bstore=0 AND store_id='" . $product['store_id'] . "' AND product_ref = '" . $product['product_ref'] . "'";
    $productLists = $db->fetchResult($sql);
    $itemGroupName = "";
    foreach ($productLists as $productList) {
        $stock = $db->fetchResult("SELECT * FROM tbl_clover_products_stock WHERE item_id = '" . $productList['id'] . "'");
        $attributes = $db->fetchResult("SELECT * FROM tbl_clover_products_attributes WHERE product_ref_id = '" . $productList['id'] . "' AND store_id='" . $product['store_id'] . "' ");
        $sql = "SELECT * FROM tbl_clover_products_groups WHERE id='" . $productList['item_group_id'] . "'";
        $itemGroup = $db->fetchResult($sql);
        $optionNames = [];
        $attributeLists = [];
        if (!empty($itemGroup)) {
            $itemGroupName = $itemGroup[0]['name'];
            $variationLists = [];
            foreach ($attributes as $attribute) {
                $attributeLists[] = ['product_option_name' => $attribute['name']];
                $options = $db->fetchResult("SELECT * FROM tbl_clover_products_options WHERE attribute_id = '" . $attribute['id'] . "' ORDER BY id ASC LIMIT 1");
                if (empty($options))
                    continue;
                $optionNames[] = $options[0]['name'];
            }
        }
            $variations[] = [
                'name' => (!empty($optionNames) ? implode(",", $optionNames) : ""),
                'location_name' => "Main store",
                "weight" => "",
                "height" => "",
                "length" => "",
                "width" => "",
                "barcode" => $productList['code'],
                "reorder_point" => "",
                "item_details" => [
                    "lot_remaining" => (!empty($stock) ? $stock[0]['stock_count'] : 0),
                    "cogs_per_unit" => "",
                    "offer_price" => ($productList['price'] / 100),
                    "commission" => "",
                    "wholesale_price" => ($productList['price'] / 100),
                    "unit_cost" => ($productList['cost'] == 0 ? "" : $productList['cost']),
//                            "list_price" => ($productList['price'] / 100),
                    "warehouse_bin" => "",
                    "low_threshold" => "",
                ],
            ];

    }
    $merchants[$product['clover_auth_id']][$product['store_id']][$product['product_ref']][] = [
        "Legacy SKU" => "",
        "Product Title" => (!empty($itemGroupName) ? $itemGroupName : $product['name']),
        "Product Category" => [
            [
                "name" => "",
            ]
        ],
        "Tax - exempt" => "",
        "Make" => "",
        "Model" => "",
        "Summary Description" => "",
        "Detailed Description" => "",
        "Options" => $attributeLists,
        "Variants" => $variations,
        "Ship to customer" => "",
        "Pickup in store" => "",
        "Charge by Weight" => "",
        "Accessories Title" => "",
        "Accessories Limit" => "",
        "Accessories Position" => "",
        "Accessory" => $modiferLists
    ];
}
//$merchantId = $;
foreach ($merchants as $merchantAuthId => $merchant) {
    echo "clover merchant auth id - " . $merchantAuthId . "\n";
    $sql = "SELECT * FROM tbl_clover_authentications WHERE id = '" . $merchantAuthId . "'";
    $authMerchant = $db->fetchResult($sql);
    if (empty($authMerchant)) {
        continue;
    }
    $merchantId = $authMerchant[0]['w3b_merchant_id'];
    $apiKey = $authMerchant[0]['w3b_api_key'];
    foreach ($merchant as $storeId => $stores) {
        foreach ($stores as $productRef => $storeProducts) {
            foreach ($storeProducts as $product) {
                echo "Need to test in  live server\n";
                die;
                $requestBody[] = $product;
                $requestBody = json_encode($requestBody);
                echo $requestBody;
                die;
//                $w3bstore = new W3bstoreAPI($storeId, $merchantId, $apiKey);
//                $response = $w3bstore->request($requestBody, 'add_items', 'POST');
                $response = '[ 
   { 
      "error":0,
      "response":{ 
         "result":"OK",
         "ids":[ 
            { 
               "id":48910,
               "url":"https://salon2.kazzarbazzar.com/48910",
               "name":"test product with three options",
               "type":"Main item"
            },
            { 
               "id":48911,
               "url":"https://salon2.kazzarbazzar.com/48911",
               "name":"Lulus Merope Light Brown Striped Oversized Scarf",
               "type":"Accessory"
            },
            { 
               "id":48912,
               "url":"https://salon2.kazzarbazzar.com/48912",
               "name":"Lulus Spirit of the Wild Gold Layered Necklace",
               "type":"Accessory"
            }
         ]
      }
   }
]';
                $response = json_decode($response, true);
                $response = $response[0];
                if ($response['error'] == 1) {
                    echo json_encode($response);
                }
                $response = $response['response'];
                if ($response['result'] == 'OK' || $response['result'] == 'ok' && !empty($response['ids'])) {
                    $ids = $response['ids'][0]['id'];
                    $db->executeQuery("UPDATE tbl_clover_products SET member_item_id = $ids");
                }
            }
        }

    }
}