<?php
/**
 * GetItems
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * W3bStore API
 *
 * W3BStore API
 *
 * OpenAPI spec version: 1.0.0
 * Contact: apiteam@w3bstore.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.9
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Swagger\Client\Model;

use \ArrayAccess;
use \Swagger\Client\ObjectSerializer;

/**
 * GetItems Class Doc Comment
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class GetItems implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $swaggerModelName = 'getItems';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = [
        'item_number' => 'int',
        'accessory' => '\Swagger\Client\Model\GetAccessory',
        'related' => 'string',
        'legacy_sku' => 'string',
        'product_title' => 'string',
        'product_category' => '\Swagger\Client\Model\GetCategory',
        'tax_exempt' => 'string',
        'make' => 'string',
        'model' => 'string',
        'summary_description' => 'string',
        'detailed_description' => 'string',
        'options' => '\Swagger\Client\Model\GetOptions',
        'variants' => '\Swagger\Client\Model\GetVariants',
        'images' => 'string[]',
        'ship_to_customer' => 'string',
        'pickup_in_store' => 'string',
        'charge_by_weight' => 'string',
        'accessories_title' => 'string',
        'accessories_limit' => 'int',
        'accessories_position' => 'int'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerFormats = [
        'item_number' => 'int64',
        'accessory' => null,
        'related' => null,
        'legacy_sku' => null,
        'product_title' => null,
        'product_category' => null,
        'tax_exempt' => null,
        'make' => null,
        'model' => null,
        'summary_description' => null,
        'detailed_description' => null,
        'options' => null,
        'variants' => null,
        'images' => null,
        'ship_to_customer' => null,
        'pickup_in_store' => null,
        'charge_by_weight' => null,
        'accessories_title' => null,
        'accessories_limit' => null,
        'accessories_position' => null
    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'item_number' => 'item_number',
        'accessory' => 'Accessory',
        'related' => 'Related',
        'legacy_sku' => 'Legacy SKU',
        'product_title' => 'Product Title',
        'product_category' => 'Product Category',
        'tax_exempt' => 'Tax-exempt',
        'make' => 'Make',
        'model' => 'Model',
        'summary_description' => 'Summary Description',
        'detailed_description' => 'Detailed Description',
        'options' => 'Options',
        'variants' => 'Variants',
        'images' => 'Images',
        'ship_to_customer' => 'Ship to customer',
        'pickup_in_store' => 'Pickup in store',
        'charge_by_weight' => 'Charge by Weight',
        'accessories_title' => 'Accessories Title',
        'accessories_limit' => 'Accessories Limit',
        'accessories_position' => 'Accessories Position'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'item_number' => 'setItemNumber',
        'accessory' => 'setAccessory',
        'related' => 'setRelated',
        'legacy_sku' => 'setLegacySku',
        'product_title' => 'setProductTitle',
        'product_category' => 'setProductCategory',
        'tax_exempt' => 'setTaxExempt',
        'make' => 'setMake',
        'model' => 'setModel',
        'summary_description' => 'setSummaryDescription',
        'detailed_description' => 'setDetailedDescription',
        'options' => 'setOptions',
        'variants' => 'setVariants',
        'images' => 'setImages',
        'ship_to_customer' => 'setShipToCustomer',
        'pickup_in_store' => 'setPickupInStore',
        'charge_by_weight' => 'setChargeByWeight',
        'accessories_title' => 'setAccessoriesTitle',
        'accessories_limit' => 'setAccessoriesLimit',
        'accessories_position' => 'setAccessoriesPosition'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'item_number' => 'getItemNumber',
        'accessory' => 'getAccessory',
        'related' => 'getRelated',
        'legacy_sku' => 'getLegacySku',
        'product_title' => 'getProductTitle',
        'product_category' => 'getProductCategory',
        'tax_exempt' => 'getTaxExempt',
        'make' => 'getMake',
        'model' => 'getModel',
        'summary_description' => 'getSummaryDescription',
        'detailed_description' => 'getDetailedDescription',
        'options' => 'getOptions',
        'variants' => 'getVariants',
        'images' => 'getImages',
        'ship_to_customer' => 'getShipToCustomer',
        'pickup_in_store' => 'getPickupInStore',
        'charge_by_weight' => 'getChargeByWeight',
        'accessories_title' => 'getAccessoriesTitle',
        'accessories_limit' => 'getAccessoriesLimit',
        'accessories_position' => 'getAccessoriesPosition'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$swaggerModelName;
    }

    const TAX_EXEMPT_Y = 'Y';
    const TAX_EXEMPT_N = 'N';
    const SHIP_TO_CUSTOMER_X = 'x';
    const SHIP_TO_CUSTOMER_EMPTY = '';
    const PICKUP_IN_STORE_X = 'x';
    const PICKUP_IN_STORE_EMPTY = '';
    const CHARGE_BY_WEIGHT_X = 'x';
    const CHARGE_BY_WEIGHT_EMPTY = '';
    

    
    /**
     * Gets allowable values of the enum
     *
     * @return string[]
     */
    public function getTaxExemptAllowableValues()
    {
        return [
            self::TAX_EXEMPT_Y,
            self::TAX_EXEMPT_N,
        ];
    }
    
    /**
     * Gets allowable values of the enum
     *
     * @return string[]
     */
    public function getShipToCustomerAllowableValues()
    {
        return [
            self::SHIP_TO_CUSTOMER_X,
            self::SHIP_TO_CUSTOMER_EMPTY,
        ];
    }
    
    /**
     * Gets allowable values of the enum
     *
     * @return string[]
     */
    public function getPickupInStoreAllowableValues()
    {
        return [
            self::PICKUP_IN_STORE_X,
            self::PICKUP_IN_STORE_EMPTY,
        ];
    }
    
    /**
     * Gets allowable values of the enum
     *
     * @return string[]
     */
    public function getChargeByWeightAllowableValues()
    {
        return [
            self::CHARGE_BY_WEIGHT_X,
            self::CHARGE_BY_WEIGHT_EMPTY,
        ];
    }
    

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['item_number'] = isset($data['item_number']) ? $data['item_number'] : null;
        $this->container['accessory'] = isset($data['accessory']) ? $data['accessory'] : null;
        $this->container['related'] = isset($data['related']) ? $data['related'] : null;
        $this->container['legacy_sku'] = isset($data['legacy_sku']) ? $data['legacy_sku'] : null;
        $this->container['product_title'] = isset($data['product_title']) ? $data['product_title'] : null;
        $this->container['product_category'] = isset($data['product_category']) ? $data['product_category'] : null;
        $this->container['tax_exempt'] = isset($data['tax_exempt']) ? $data['tax_exempt'] : null;
        $this->container['make'] = isset($data['make']) ? $data['make'] : null;
        $this->container['model'] = isset($data['model']) ? $data['model'] : null;
        $this->container['summary_description'] = isset($data['summary_description']) ? $data['summary_description'] : null;
        $this->container['detailed_description'] = isset($data['detailed_description']) ? $data['detailed_description'] : null;
        $this->container['options'] = isset($data['options']) ? $data['options'] : null;
        $this->container['variants'] = isset($data['variants']) ? $data['variants'] : null;
        $this->container['images'] = isset($data['images']) ? $data['images'] : null;
        $this->container['ship_to_customer'] = isset($data['ship_to_customer']) ? $data['ship_to_customer'] : null;
        $this->container['pickup_in_store'] = isset($data['pickup_in_store']) ? $data['pickup_in_store'] : null;
        $this->container['charge_by_weight'] = isset($data['charge_by_weight']) ? $data['charge_by_weight'] : null;
        $this->container['accessories_title'] = isset($data['accessories_title']) ? $data['accessories_title'] : null;
        $this->container['accessories_limit'] = isset($data['accessories_limit']) ? $data['accessories_limit'] : null;
        $this->container['accessories_position'] = isset($data['accessories_position']) ? $data['accessories_position'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        $allowedValues = $this->getTaxExemptAllowableValues();
        if (!is_null($this->container['tax_exempt']) && !in_array($this->container['tax_exempt'], $allowedValues, true)) {
            $invalidProperties[] = sprintf(
                "invalid value for 'tax_exempt', must be one of '%s'",
                implode("', '", $allowedValues)
            );
        }

        $allowedValues = $this->getShipToCustomerAllowableValues();
        if (!is_null($this->container['ship_to_customer']) && !in_array($this->container['ship_to_customer'], $allowedValues, true)) {
            $invalidProperties[] = sprintf(
                "invalid value for 'ship_to_customer', must be one of '%s'",
                implode("', '", $allowedValues)
            );
        }

        $allowedValues = $this->getPickupInStoreAllowableValues();
        if (!is_null($this->container['pickup_in_store']) && !in_array($this->container['pickup_in_store'], $allowedValues, true)) {
            $invalidProperties[] = sprintf(
                "invalid value for 'pickup_in_store', must be one of '%s'",
                implode("', '", $allowedValues)
            );
        }

        $allowedValues = $this->getChargeByWeightAllowableValues();
        if (!is_null($this->container['charge_by_weight']) && !in_array($this->container['charge_by_weight'], $allowedValues, true)) {
            $invalidProperties[] = sprintf(
                "invalid value for 'charge_by_weight', must be one of '%s'",
                implode("', '", $allowedValues)
            );
        }

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets item_number
     *
     * @return int
     */
    public function getItemNumber()
    {
        return $this->container['item_number'];
    }

    /**
     * Sets item_number
     *
     * @param int $item_number item_number
     *
     * @return $this
     */
    public function setItemNumber($item_number)
    {
        $this->container['item_number'] = $item_number;

        return $this;
    }

    /**
     * Gets accessory
     *
     * @return \Swagger\Client\Model\GetAccessory
     */
    public function getAccessory()
    {
        return $this->container['accessory'];
    }

    /**
     * Sets accessory
     *
     * @param \Swagger\Client\Model\GetAccessory $accessory accessory
     *
     * @return $this
     */
    public function setAccessory($accessory)
    {
        $this->container['accessory'] = $accessory;

        return $this;
    }

    /**
     * Gets related
     *
     * @return string
     */
    public function getRelated()
    {
        return $this->container['related'];
    }

    /**
     * Sets related
     *
     * @param string $related Array of related products id's
     *
     * @return $this
     */
    public function setRelated($related)
    {
        $this->container['related'] = $related;

        return $this;
    }

    /**
     * Gets legacy_sku
     *
     * @return string
     */
    public function getLegacySku()
    {
        return $this->container['legacy_sku'];
    }

    /**
     * Sets legacy_sku
     *
     * @param string $legacy_sku legacy_sku
     *
     * @return $this
     */
    public function setLegacySku($legacy_sku)
    {
        $this->container['legacy_sku'] = $legacy_sku;

        return $this;
    }

    /**
     * Gets product_title
     *
     * @return string
     */
    public function getProductTitle()
    {
        return $this->container['product_title'];
    }

    /**
     * Sets product_title
     *
     * @param string $product_title product_title
     *
     * @return $this
     */
    public function setProductTitle($product_title)
    {
        $this->container['product_title'] = $product_title;

        return $this;
    }

    /**
     * Gets product_category
     *
     * @return \Swagger\Client\Model\GetCategory
     */
    public function getProductCategory()
    {
        return $this->container['product_category'];
    }

    /**
     * Sets product_category
     *
     * @param \Swagger\Client\Model\GetCategory $product_category product_category
     *
     * @return $this
     */
    public function setProductCategory($product_category)
    {
        $this->container['product_category'] = $product_category;

        return $this;
    }

    /**
     * Gets tax_exempt
     *
     * @return string
     */
    public function getTaxExempt()
    {
        return $this->container['tax_exempt'];
    }

    /**
     * Sets tax_exempt
     *
     * @param string $tax_exempt tax_exempt
     *
     * @return $this
     */
    public function setTaxExempt($tax_exempt)
    {
        $allowedValues = $this->getTaxExemptAllowableValues();
        if (!is_null($tax_exempt) && !in_array($tax_exempt, $allowedValues, true)) {
            throw new \InvalidArgumentException(
                sprintf(
                    "Invalid value for 'tax_exempt', must be one of '%s'",
                    implode("', '", $allowedValues)
                )
            );
        }
        $this->container['tax_exempt'] = $tax_exempt;

        return $this;
    }

    /**
     * Gets make
     *
     * @return string
     */
    public function getMake()
    {
        return $this->container['make'];
    }

    /**
     * Sets make
     *
     * @param string $make make
     *
     * @return $this
     */
    public function setMake($make)
    {
        $this->container['make'] = $make;

        return $this;
    }

    /**
     * Gets model
     *
     * @return string
     */
    public function getModel()
    {
        return $this->container['model'];
    }

    /**
     * Sets model
     *
     * @param string $model model
     *
     * @return $this
     */
    public function setModel($model)
    {
        $this->container['model'] = $model;

        return $this;
    }

    /**
     * Gets summary_description
     *
     * @return string
     */
    public function getSummaryDescription()
    {
        return $this->container['summary_description'];
    }

    /**
     * Sets summary_description
     *
     * @param string $summary_description summary_description
     *
     * @return $this
     */
    public function setSummaryDescription($summary_description)
    {
        $this->container['summary_description'] = $summary_description;

        return $this;
    }

    /**
     * Gets detailed_description
     *
     * @return string
     */
    public function getDetailedDescription()
    {
        return $this->container['detailed_description'];
    }

    /**
     * Sets detailed_description
     *
     * @param string $detailed_description detailed_description
     *
     * @return $this
     */
    public function setDetailedDescription($detailed_description)
    {
        $this->container['detailed_description'] = $detailed_description;

        return $this;
    }

    /**
     * Gets options
     *
     * @return \Swagger\Client\Model\GetOptions
     */
    public function getOptions()
    {
        return $this->container['options'];
    }

    /**
     * Sets options
     *
     * @param \Swagger\Client\Model\GetOptions $options options
     *
     * @return $this
     */
    public function setOptions($options)
    {
        $this->container['options'] = $options;

        return $this;
    }

    /**
     * Gets variants
     *
     * @return \Swagger\Client\Model\GetVariants
     */
    public function getVariants()
    {
        return $this->container['variants'];
    }

    /**
     * Sets variants
     *
     * @param \Swagger\Client\Model\GetVariants $variants variants
     *
     * @return $this
     */
    public function setVariants($variants)
    {
        $this->container['variants'] = $variants;

        return $this;
    }

    /**
     * Gets images
     *
     * @return string[]
     */
    public function getImages()
    {
        return $this->container['images'];
    }

    /**
     * Sets images
     *
     * @param string[] $images images
     *
     * @return $this
     */
    public function setImages($images)
    {
        $this->container['images'] = $images;

        return $this;
    }

    /**
     * Gets ship_to_customer
     *
     * @return string
     */
    public function getShipToCustomer()
    {
        return $this->container['ship_to_customer'];
    }

    /**
     * Sets ship_to_customer
     *
     * @param string $ship_to_customer ship_to_customer
     *
     * @return $this
     */
    public function setShipToCustomer($ship_to_customer)
    {
        $allowedValues = $this->getShipToCustomerAllowableValues();
        if (!is_null($ship_to_customer) && !in_array($ship_to_customer, $allowedValues, true)) {
            throw new \InvalidArgumentException(
                sprintf(
                    "Invalid value for 'ship_to_customer', must be one of '%s'",
                    implode("', '", $allowedValues)
                )
            );
        }
        $this->container['ship_to_customer'] = $ship_to_customer;

        return $this;
    }

    /**
     * Gets pickup_in_store
     *
     * @return string
     */
    public function getPickupInStore()
    {
        return $this->container['pickup_in_store'];
    }

    /**
     * Sets pickup_in_store
     *
     * @param string $pickup_in_store pickup_in_store
     *
     * @return $this
     */
    public function setPickupInStore($pickup_in_store)
    {
        $allowedValues = $this->getPickupInStoreAllowableValues();
        if (!is_null($pickup_in_store) && !in_array($pickup_in_store, $allowedValues, true)) {
            throw new \InvalidArgumentException(
                sprintf(
                    "Invalid value for 'pickup_in_store', must be one of '%s'",
                    implode("', '", $allowedValues)
                )
            );
        }
        $this->container['pickup_in_store'] = $pickup_in_store;

        return $this;
    }

    /**
     * Gets charge_by_weight
     *
     * @return string
     */
    public function getChargeByWeight()
    {
        return $this->container['charge_by_weight'];
    }

    /**
     * Sets charge_by_weight
     *
     * @param string $charge_by_weight charge_by_weight
     *
     * @return $this
     */
    public function setChargeByWeight($charge_by_weight)
    {
        $allowedValues = $this->getChargeByWeightAllowableValues();
        if (!is_null($charge_by_weight) && !in_array($charge_by_weight, $allowedValues, true)) {
            throw new \InvalidArgumentException(
                sprintf(
                    "Invalid value for 'charge_by_weight', must be one of '%s'",
                    implode("', '", $allowedValues)
                )
            );
        }
        $this->container['charge_by_weight'] = $charge_by_weight;

        return $this;
    }

    /**
     * Gets accessories_title
     *
     * @return string
     */
    public function getAccessoriesTitle()
    {
        return $this->container['accessories_title'];
    }

    /**
     * Sets accessories_title
     *
     * @param string $accessories_title accessories_title
     *
     * @return $this
     */
    public function setAccessoriesTitle($accessories_title)
    {
        $this->container['accessories_title'] = $accessories_title;

        return $this;
    }

    /**
     * Gets accessories_limit
     *
     * @return int
     */
    public function getAccessoriesLimit()
    {
        return $this->container['accessories_limit'];
    }

    /**
     * Sets accessories_limit
     *
     * @param int $accessories_limit accessories_limit
     *
     * @return $this
     */
    public function setAccessoriesLimit($accessories_limit)
    {
        $this->container['accessories_limit'] = $accessories_limit;

        return $this;
    }

    /**
     * Gets accessories_position
     *
     * @return int
     */
    public function getAccessoriesPosition()
    {
        return $this->container['accessories_position'];
    }

    /**
     * Sets accessories_position
     *
     * @param int $accessories_position accessories_position
     *
     * @return $this
     */
    public function setAccessoriesPosition($accessories_position)
    {
        $this->container['accessories_position'] = $accessories_position;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}


