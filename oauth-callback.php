<?php
/**
 * Created by PhpStorm.
 * User: Salih Mohamed
 * Date: 7/31/2019
 * Time: 1:41 PM
 */
session_start();
if (!isset($_SESSION['user'])) {
//    header("Location: login.php");
//    exit;
}
include __DIR__ . "/db/Database.php";
$db = new Database();
include './CloverAPI.php';
include './vendor/autoload.php';

use GuzzleHttp\Client;

if (file_exists('./env.php')) {
    include './env.php';
}
global $ENV;
$error = 0;
$msg = "";
if (!isset($_REQUEST)) {
    $msg = "Error on Clover API OAuth Callback. Please check your clover application.";
} else {
    $merchant_id = (isset($_REQUEST['merchant_id']) ? $_REQUEST['merchant_id'] : '');
    $employee_id = (isset($_REQUEST['employee_id']) ? $_REQUEST['employee_id'] : '');
    $client_id = (isset($_REQUEST['client_id']) ? $_REQUEST['client_id'] : '');
    $code = (isset($_REQUEST['code']) ? $_REQUEST['code'] : '');
    $client_secret = $ENV['APP_SECRET'];
    $authorized_url = $ENV['AUTHORIZED_MERCHANT_URL'] . "$client_id&client_secret=$client_secret&code=" . $code;
    $_SESSION['merchant_id'] = $merchant_id;
    $_SESSION['employee_id'] = $employee_id;
    $_SESSION['client_id'] = $client_id;
    $_SESSION['code'] = $code;
    $_SESSION['access_token'] = "";
    try {
        $client = new Client();
        $response = $client->get($authorized_url);
        $responseBody = json_decode((string)$response->getBody(true));
        print_r($responseBody);
        $_SESSION['access_token'] = $responseBody->access_token;
        $error = 0;
        $msg = "";
    } catch (\Exception $exception) {
        $exception = json_decode((string)$exception->getResponse()->getBody(true));
        $error = 1;
        $msg = $exception->message;
    }
}

?>
<!doctype html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="title" content="Best eCommerce platform for multi-store retailers. Free trial."/>
    <meta name="robots" content="index, follow"/>
    <meta name="description"
          content="Online stores that help merchants grow sales and operations across online and retail channels."/>
    <meta name="keywords"
          content="e commerce,eCommerce,web store,online retail,merchant,inventory,cost of goods sold,shipping,order processing,bundles,pickup in-store, fulfillment"/>
    <meta name="language" content="en"/>
    <title>Best eCommerce platform for multi - Clover</title>
    <link rel="shortcut icon" href="/favicon.ico"/>
    <link href='https://fonts.googleapis.com/css?family=Cabin:400,600|Open+Sans:300,600,400' rel='stylesheet'>
    <link rel="stylesheet" type="text/css" media="screen" href="https://w3bstore.com/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" media="screen" href="https://w3bstore.com/css/animate.min.css"/>
    <link rel="stylesheet" type="text/css" media="screen" href="https://w3bstore.com/css/icons.css"/>
    <link rel="stylesheet" type="text/css" media="screen" href="https://w3bstore.com/css/landing.css"/>
    <link rel="stylesheet" type="text/css" media="screen" href="https://w3bstore.com/css/responsive.css"/>
    <link rel="stylesheet" type="text/css" media="screen" href="https://w3bstore.com/css/blue.css"/>
    <script src="https://w3bstore.com//js/jquery-3.3.1.min.js"></script>
    <style>
        .ulogo {
            position: relative;
            top: -12px;
            width: 150px;
            height: 40px
        }

        .sticky-navigation .navbar-brand img {
            max-width: 150px !important;
        }

        @media screen and (min-width: 340px) and (max-width: 600px) {
            .ulogo {
                top: 0px;
                width: 150px;
                height: 25px
            }

            .sticky-navigation .navbar-brand img {
                max-width: 150px !important;
            }
        }

        @media screen and (max-width: 320px) {
            .ulogo {
                top: 0px;
                width: 150px;
                height: 25px
            }

            .sticky-navigation .navbar-brand img {
                max-width: 150px !important;
            }
        }

        .clover-logo {
            width: 165px;
            height: 40px;
            margin-top: 0;
            background-image: url(https://files.readme.io/734e66d-clover-logo.svg);
        }

        .header {
            background: url("https://w3bstore.com/images/background-1.jpg") no-repeat fixed center top / cover rgba(0, 0, 0, 0);
        }

        .features {
            background: url(https://w3bstore.com/images/background-1.jpg) no-repeat top center fixed;
        }

        .stats {
            background: url(https://w3bstore.com/images/background-2.jpg) no-repeat center top fixed;
        }

        .call-to-action {
            background: url(https://w3bstore.com/images/background-2.jpg) fixed no-repeat top center;
        }
    </style>
</head>
<body data-spy="scroll" data-offset="170" data-target="#my-nav">
<div class="navbar navbar-inverse bs-docs-nav navbar-fixed-top sticky-navigation" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#stamp-navigation">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-menu"></span>
            </button>
            <a class="navbar-brand" href="#" style="margin-top: 5px;padding: 10px;">
                <img width="150" class="ulogo" src="https://w3bstore.com/images/logo_w3bstore_transparent.png" alt="">
            </a>
        </div>
    </div>
</div>
<section class="brief white-bg-border" id="section2">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <center><img style="width: 25%; margin-top: 50px" src="https://files.readme.io/734e66d-clover-logo.svg">
                </center>
                <?php
                if ($error == 0) {
                    ?>
                    <h3 class="text-center dark-text">Grant Application Access to <strong
                                style="font-weight: bolder; color: #085394">Clover</strong></h3>
                    <div class="colored-line"></div>
                    <p>Great! We need your consent to share your <b>W3bstore</b> data. Don't worry, we won't share your
                        <b>W3bstore</b> password. You can change this any time by editing your account preferences.
                        Please take the time to read <b>W3bstore</b>'s terms of service and privacy policy, because
                        these policies will apply when you use this service. Remember, we don't manage policies set by
                        other companies.
                        By clicking on the "<b>I agree</b>" button, you're allowing us to link your <b>W3bstore</b>
                        account
                        with <b>Clover</b>.
                    </p>
                    <p class="text-center">
                        <a href="oauth-success.php" type="button" class="btn btn-success">I Agree</a>
                    </p>
                    <p class="text-center">
                        <span>No thanks, take me to the <a href="#">W3bstore homepage</a></span>
                    </p>
                    <?php
                } else {
                    ?>
                    <h3 class="text-center dark-text">Grant Application Access to <strong
                                style="font-weight: bolder; color: #085394">Clover</strong></h3>
                    <div class="colored-line"></div>
                    <p><b><?= $msg ?></b></p>
                    <p class="text-center">
                        <span>Take me to the <a href="#">W3bstore Homepage</a></span>
                    </p>
                    <?php
                }
                ?>

            </div>
        </div>
    </div>
</section>
<footer class="footer grey-bg" style="clear:both;">
    ©2018 w3bstore.com LLC. All Rights Reserved
    <ul class="footer-links small-text">
        <li><a href="javascript:void(null);"
               onClick="popupWin = window.open('http://w3bstore.com/market.php/default/about', 'contacts', 'location,width=700,height=700,top=200,left=200,scrollbars=1,toolbar=no,location=no,status=no'); popupWin.focus(); return false;"
               class="dark-text">About Us</a>
        </li>
        <li><a href="javascript:void(null);"
               onClick="popupWin = window.open('http://w3bstore.com/market.php/default/privacy', 'contacts', 'location,width=700,height=700,top=200,left=200,scrollbars=1,toolbar=no,location=no,status=no'); popupWin.focus(); return false;"
               class="dark-text">Privacy</a>
        </li>
        <li><a href="javascript:void(null);"
               onClick="popupWin = window.open('http://w3bstore.com/market.php/default/terms', 'contacts', 'location,width=700,height=700,top=200,left=200,scrollbars=1,toolbar=no,location=no,status=no'); popupWin.focus(); return false;"
               class="dark-text">Terms</a>
        </li>
    </ul>
</footer>
<script src="https://w3bstore.com/js/smoothscroll.js"></script>
<script src="https://w3bstore.com/js/bootstrap.minnew.js"></script>
<script src="https://w3bstore.com/js/jquery.nav.js"></script>
<script src="https://w3bstore.com/js/wow.min.js"></script>
<script src="https://w3bstore.com/js/nivo-lightbox.min.js"></script>
<script src="https://w3bstore.com/js/owl.carousel.min.js"></script>
<script src="https://w3bstore.com/js/jquery.stellar.min.js"></script>
<script src="https://w3bstore.com/js/retina.min.js"></script>
<script src="https://w3bstore.com/js/jquery.ajaxchimp.min.js"></script>
<script src="https://w3bstore.com/js/custom.js"></script>
</body>
</html>