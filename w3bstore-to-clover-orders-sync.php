<?php
/**
 * Created by PhpStorm.
 * User: Salih Mohamed
 * Date: 7/31/2019
 * Time: 11:40 AM
 */


include __DIR__ . "/db/Database.php";
$db = new Database();
include './CloverAPI.php';
include './W3bstoreAPI.php';
include './vendor/autoload.php';
if (file_exists('./env.php')) {
    include './env.php';
}
$merchants = $db->fetchResult("SELECT * from tbl_clover_authentications WHERE location_id IS NOT NULL AND store_id IS NOT NULL AND token IS NOT NULL AND w3b_merchant_id IS NOT NULL AND merchant_id IS NOT NULL AND w3b_api_key IS NOT NULL");
foreach ($merchants as $m_key => $merchant) {
    echo $merchant['store_id'] . " ---- " . $merchant['location_id'] . " ---- " . $merchant['merchant_id'] . "\n";
    $store_id = $merchant['store_id'];
    $location_id = $merchant['location_id'];
    $CloverAPI = new W3bStore\CloverAPI();
    $W3bstoreAPI = new W3bStore\W3bstoreAPI($store_id, $merchant['w3b_merchant_id'], $merchant['w3b_api_key']);
    $orders = $W3bstoreAPI->request($ENV['W3BSTORE_API'], 'GET', $requestBody = null, 'get_orders');
    if ($orders['error'] == 0) {
        if (empty($orders['response'])) {
            echo "Orders not found\n";
            continue;
        }

        foreach ($orders['response'] as $order) {
            $mapOrderItems = [];
            $orderTotal = 0;
            $totalQty = 0;
            $hasProduct = 0;

            if (empty($order['items'])) {
                echo "Ordered items cannot be empty\n";
                continue;
            }
            foreach ($order['items'] as $key => $orderItem) {
                $cloverProducts = $db->fetchResult("SELECT * from tbl_clover_products WHERE  clover_auth_id = '" . $merchant['id'] . "' AND member_item_id ='" . $orderItem['item_id'] . "' ");
                if (!empty($cloverProducts)) {
                    $orderTotal += $orderItem['offer_price'] * $orderItem['qty_fulfilled'];
                    $totalQty += $orderItem['qty_fulfilled'];
                    $order['items'][$key]['clover_id'] = $cloverProducts[0]['clover_id'];
                    $order['items'][$key]['name'] = $cloverProducts[0]['name'];
                    $hasProduct++;
                }
            }
            if (count($order['items']) != $hasProduct) {
                echo "Ordered items not matched to w3bstore products\n";
                continue;
            }
            echo "Order id :- " . $order['order_id'] . "\n";
            $customerCloverId = null;
            $customerId = null;
            if (!empty($order['customer_info']) && !empty($order['customer_id'])) {
                $customerDetils = $order['customer_info'];
                $hasCustomer = $db->fetchResult("SELECT * FROM tbl_clover_customers WHERE customer_id = '" . $order['customer_id'] . "' AND clover_auth_id = '" . $merchant['id'] . "'");
                if (empty($hasCustomer)) {
                    $customerRequestBody = [
                        "firstName" => $customerDetils['first_name'],
                        "lastName" => $customerDetils['last_name'],
                        "marketingAllowed" => false,
                        "customerSince" => (!empty($customerDetils['created_at']) ? strtotime(date('Y-m-d', strtotime($customerDetils['created_at']))) : null),
                        "addresses" => [
                            [
                                "address1" => $customerDetils['street'],
                                "address2" => $customerDetils['street2'],
                                "address3" => "",
                                "city" => $customerDetils['city'],
                                "country" => "US",
                                "phoneNumber" => $customerDetils['phone'],
                                "state" => $customerDetils['state'],
                                "zip" => $customerDetils['zip'],
                            ]
                        ],
                        "emailAddresses" => [
                            [
                                "emailAddress" => $customerDetils['email'],
                            ]
                        ],
                        "phoneNumbers" => [
                            [
                                "phoneNumber" => $customerDetils['phone']
                            ],
                        ],
                        "metadata" => [
                            "businessName" => "",
                            "note" => "",
                            "dobYear" => "",
                            "dobMonth" => "",
                            "dobDay" => "",
                        ]
                    ];
                    $response = $CloverAPI->addNewCustomer($customerRequestBody);
                    if (!empty($response->id)) {
                        //customer created successfully
                        $sql = "INSERT INTO `tbl_clover_customers` (`id`, `clover_auth_id`,`clover_id`,`first_name`,`last_name`,`customer_since`,`customer_id`) VALUES(NULL, '" . $merchant['id'] . "', '" . $response->id . "','" . $customerDetils['first_name'] . "','" . $customerDetils['last_name'] . "','" . (!empty($customerDetils['created_at']) ? strtotime(date('Y-m-d', strtotime($customerDetils['created_at']))) : null) . "','" . $order['customer_id'] . "')";
                        $db->executeQuery($sql);
                        $customerId = $db->lastInsetedId();
                        $customerCloverId = $response->id;
                    } else {
                        $W3bstoreAPI->logApiError($merchant['id'], 'clover-add_customer', $customerRequestBody, $response, (isset($response->message) ? $response->message : ''));
                        echo "error- " . (isset($response->message) ? $response->message : '') . "\n";
                        continue;
                    }
                } else {
                    $customerId = $hasCustomer[0]['id'];
                    $customerCloverId = $hasCustomer[0]['clover_id'];
                }
            }
            echo "Customer clover id - " . $customerCloverId . "\n";
            $hasOrder = $db->fetchResult("SELECT * FROM tbl_clover_orders WHERE clover_auth_id ='" . $merchant['id'] . "' AND order_id = '" . $order['order_id'] . "' ");
            if (empty($hasOrder)) {
                $orderRequestBody = [
                    "currency" => "USD",
                    "total" => ($orderTotal * 100),
                    "externalReferenceId" => $order['order_id'],
                    "paymentState" => 1,
                    "title" => "",
                    "note" => $order['notes'],
                    "testMode" => "true",
                    "taxRemoved" => "",
                    "isVat" => "",
                    "state" => "open",
                    "paymentState" => "PAID",
                    "manualTransaction" => "",
                    "groupLineItems" => true,
                    "testMode" => true,
                    "payType" => "FULL",
                    "createdTime" => strtotime(date("Y-m-d h:i:s", strtotime($order['order_date']))),
                    "clientCreatedTime" => strtotime(date("Y-m-d h:i:s", strtotime($order['order_date']))),
                    "modifiedTime" => strtotime(date("Y-m-d h:i:s", strtotime($order['order_date']))),
                ];
                $response = $CloverAPI->addOrders($merchant, $orderRequestBody);
                if (!empty($response->id)) {
                    if (!empty($customerCloverId)) {
                        //add customer to order
                        $customerRequestBody = [
                            "customers" => [
                                [
                                    "id" => $customerCloverId
                                ]
                            ]
                        ];
                        $CloverAPI->updateOrder($merchant, $response->id, $customerRequestBody);
                    }
                    $sql = "INSERT INTO `tbl_clover_orders` ( `clover_auth_id`, `clover_id`, `currency`, `customer_id`,`total`,`tax_removed`,`is_vat`, `state`, `manual_transaction`, `group_line_items`, `test_mode`, `created_time`, `client_created_time`, `modified_time`, `order_id`) VALUES ('" . $merchant['id'] . "','" . $response->id . "', '" . (isset($response->currency) ? $response->currency : '') . "', null, '" . (isset($response->total) ? $response->total / 100 : 0) . "', '" . (isset($response->taxRemoved) ? $response->taxRemoved : '') . "', '" . (isset($response->isVat) ? $response->isVat : '') . "', '" . (isset($response->state) ? $response->state : '') . "', '" . (isset($response->manualTransaction) ? $response->manualTransaction : '') . "', '" . (isset($response->groupLineItems) ? $response->groupLineItems : '') . "', '" . (isset($response->testMode) ? $response->testMode : '') . "', '" . $response->createdTime . "',  '" . $response->clientCreatedTime . "', '" . $response->modifiedTime . "', '" . $order['order_id'] . "')";
                    $db->executeQuery($sql);
                    $orderId = $db->lastInsetedId();
                    $items = [];
                    $productTitles = [];
                    foreach ($order['items'] as $orderItem) {
                        if (empty($orderItem['clover_id']))
                            continue;
                        $productTitles[$orderItem['clover_id']] = $orderItem['item_title'];
                        $items['items'][] = ['item' => ['id' => $orderItem['clover_id']], 'name' => $orderItem['item_title'], 'unitQty' => (int)$orderItem['qty_fulfilled'] * 1000, 'price' => $orderItem['offer_price'] * 100];
                    }
                    $responses = $CloverAPI->addOrderLineItems($merchant, $response->id, $items);
                    if (!empty($responses) && empty($responses->message)) {
                        foreach ($responses as $response) {
                            $db->executeQuery("INSERT INTO `tbl_clover_ordered_products` (`order_id`, `clover_id`, `clover_item_id`, `name`, `price`, `qty`, `item_code`, `printed`, `created_time`, `order_client_created_time`, `exchanged`, `refunded`, `is_revenue`) 
                            VALUES ('$orderId', '" . $response->id . "',  '" . $response->item->id . "', '" . (!empty($productTitles[$response->item->id]) ? addslashes($productTitles[$response->item->id]) : null) . "', '" . (isset($response->price) ? ($response->price / 100) : 0) . "','" . ($response->unitQty / 1000) . "', '" . (isset($response->itemCode) ? $response->itemCode : '') . "', '" . (isset($response->printed) ? $response->printed : '') . "', '" . $response->createdTime . "', '" . $response->orderClientCreatedTime . "', '" . (isset($response->exchanged) ? $response->exchanged : '') . "', '" . (isset($response->refunded) ? $response->refunded : '') . "', '" . (isset($response->isRevenue) ? $response->isRevenue : '') . "')");
                            $orderItemId = $db->lastInsetedId();
                            echo "Order line item clover id - " . $response->id . "\n";
                        }
                    } else {
                        $W3bstoreAPI->logApiError($merchant['id'], 'clover-add_order_line_items', $items, $responses, (isset($responses->message) ? $responses->message : ''));
                        echo "error- " . (isset($responses->message) ? $responses->message : '') . "\n";
                        continue;
                    }
                } else {
                    $W3bstoreAPI->logApiError($merchant['id'], 'clover-add_order', $orderRequestBody, $response, (isset($response->message) ? $response->message : ''));
                    echo "error- " . (isset($response->message) ? $response->message : '') . "\n";
                    continue;
                }
            } else {
                $orderId = $hasOrder[0]['id'];
            }
            $db->executeQuery("UPDATE tbl_clover_orders SET customer_id = '" . $customerId . "', total_qty = '" . $totalQty . "' WHERE id='" . $orderId . "'");
        }
    } else {
        $W3bstoreAPI->logApiError($merchant['id'], 'w3bstore-get_orders', '', $orders, (isset($orders['msg']) ? $orders['msg'] : ""));
    }
}