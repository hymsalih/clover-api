<?php
/**
 * Created by PhpStorm.
 * User: Salih Mohamed
 * Date: 7/31/2019
 * Time: 11:40 AM
 */
set_time_limit(0);
include __DIR__ . "/db/Database.php";
$db = new Database();
include './CloverAPI.php';
include './W3bstoreAPI.php';
include './vendor/autoload.php';
require_once(__DIR__ . '/vendor/autoload.php');
if (file_exists('./env.php')) {
    include './env.php';
}
$CloverAPI = new W3bStore\CloverAPI();
$merchants = $db->fetchResult("SELECT * from tbl_clover_authentications");
foreach ($merchants as $merchant) {
    $orders = $CloverAPI->readOrders($merchant, ['lineItems', 'serviceCharge', 'discounts', 'credits', 'payments', 'refunds'], ['limit' => 1000]);
    if (!isset($orders->elements))
        continue;

    echo count($orders->elements) . "\n";
    foreach ($orders->elements as $p_key => $order) {
        $response = $CloverAPI->deleteOrder($order->id);
    }
}
echo "\n\nDone!";
