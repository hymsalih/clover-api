<?php
/**
 * Created by PhpStorm.
 * User: Salih Mohamed
 * Date: 7/31/2019
 * Time: 11:40 AM
 */
set_time_limit(0);
include __DIR__ . "/db/Database.php";
$db = new Database();
include './CloverAPI.php';
include './W3bstoreAPI.php';
include './vendor/autoload.php';
require_once(__DIR__ . '/vendor/autoload.php');
if (file_exists('./env.php')) {
    include './env.php';
}
$CloverAPI = new W3bStore\CloverAPI();
$merchants = $db->fetchResult("SELECT * from tbl_clover_authentications WHERE location_id IS NOT NULL AND store_id IS NOT NULL AND token IS NOT NULL AND w3b_merchant_id IS NOT NULL AND merchant_id IS NOT NULL AND w3b_api_key IS NOT NULL");
$cloverProducts = [];
foreach ($merchants as $merchant) {
    $locationId = $merchant['location_id'];
    if (empty($locationId)) {
        echo "Invalid store location\n";
        continue;
    }
    $locationName = $db->fetchResult("SELECT * from tbl_member_store_shipping_suppliers WHERE id = $locationId");
    if (empty($locationName)) {
        echo "Invalid location name\n";
        continue;
    }
    $locationName = $locationName[0]['warehouse_supplier_name'];
    $products = $CloverAPI->readProducts($merchant, ['tags', 'categories', 'taxRates', 'modifierGroups', 'itemGroup', 'itemStock', 'options'], ['limit' => 1000]);
    if (!isset($products->elements)) {
        echo(isset($products->message) ? $products->message . "\n\n" : "\n\n");
        continue;
    }
    echo $merchant['store_id'] . "----" . $merchant['store_name'] . "----" . $locationName . "\n";
    $importedProducts = '';
    foreach ($products->elements as $p_key => $product) {
        if (empty($product->name))
            continue;
        $sql = "SELECT * FROM `tbl_clover_products` where clover_id = '" . $product->id . "' AND store_id='" . $merchant['store_id'] . "' AND location_id=$locationId";
        $productRec = $db->fetchResult($sql);
        $itemGroupId = "";
        if (empty($productRec)) {
            $sql = "INSERT INTO `tbl_clover_products` (`id`, `clover_auth_id`, `store_id`, `member_item_id`, `mkp_item_id`, `clover_id`, `hidden`, `name`,`code`,`sku`,`cost`, `price`, `price_type`, `default_tax_rate`, `is_revenue`, `in_clover`, `location_id`, `status`, `item_group_id`) VALUES (NULL, '" . $merchant['id'] . "','" . $merchant['store_id'] . "', null, null, '" . $product->id . "', '" . $product->hidden . "', '" . addslashes($product->name) . "', '" . (isset($product->code) ? $product->code : null) . "', '" . (isset($product->sku) ? $product->sku : null) . "', '" . (isset($product->cost) ? $product->cost : null) . "', '" . $product->price . "', '" . $product->priceType . "',  '" . $product->defaultTaxRates . "', '" . $product->isRevenue . "', 1,'" . $locationId . "','UPDATE PRICES FALSE',NULL)";
            $db->executeQuery($sql);
            $productId = $db->lastInsetedId();
        } else {
            $productId = $productRec[0]['id'];
            if ($product->price != $productRec[0]['price']) {
                $db->executeQuery("UPDATE `tbl_clover_products` SET status ='UPDATE W3BSTORE PRICE TRUE'  WHERE id='" . $productRec[0]['id'] . "'");
            }
        }
        echo "Product------------------" . $productId . " - " . $product->name . " - " . $product->id . "\n";

        if (isset($product->itemStock)) {
            $sql = "SELECT * FROM tbl_clover_products_stock WHERE item_id='" . $productId . "'";
            $hasStock = $db->fetchResult($sql);
            if (empty($hasStock)) {
                $sql = "INSERT INTO `tbl_clover_products_stock` (`id`, `item_id`, `stock_count`, `quantity`) VALUES (NULL, '$productId', '" . $product->itemStock->stockCount . "', '" . $product->itemStock->quantity . "')";
                $db->executeQuery($sql);
                $stockId = $db->lastInsetedId();
            } else {
                $stockId = $hasStock[0]['id'];
                if ($hasStock[0]['quantity'] != $product->itemStock->quantity) {
                    $sql = "UPDATE `tbl_clover_products_stock` SET status ='UPDATE W3BSTORE STOCK TRUE', stock_count='" . $product->itemStock->quantity . "' ,quantity='" . $product->itemStock->quantity . "'  WHERE id=$stockId";
                    $db->executeQuery($sql);
                }
            }
            echo "Stock-------------------------------------------------------------------------" . $stockId . " - " . $product->itemStock->stockCount . " - " . $product->itemStock->quantity . "\n";
        }
        $modifierGroupId = 'NULL';
        if (!empty($product->modifierGroups->elements)) {
            $modifierGroupId = time();
            foreach ($product->modifierGroups->elements as $modifierGroup) {
                $modifierGroupRefId = null;
                $hasModifierGroup = $db->fetchResult("SELECT * FROM tbl_clover_modifier_group WHERE clover_id = '" . $modifierGroup->id . "' AND store_id='" . $merchant['store_id'] . "'");
                if (empty($hasModifierGroup)) {
                    $sql = "INSERT INTO `tbl_clover_modifier_group` (`id`, `store_id`, `ref_id`, `title`, `clover_id`, `min_price`, `max_price`, `price`, `location_id`) VALUES(NULL, '" . $merchant['store_id'] . "','" . $modifierGroupId . "', '" . addslashes($modifierGroup->name) . "','" . $modifierGroup->id . "','" . (!empty($modifierGroup->minRequired) ? (int)$modifierGroup->minRequired : 0) . "','" . (int)$modifierGroup->maxAllowed . "','" . (int)$modifierGroup->maxAllowed . "',,  '" . (isset($itemGroup->id) ? $itemGroup->id : '') . "',$locationId)";
                    $db->executeQuery($sql);
                    $modifierGroupRefId = $db->lastInsetedId();
                } else {
                    $modifierGroupId = $hasModifierGroup[0]['ref_id'];
                    $modifierGroupRefId = $hasModifierGroup[0]['id'];
                }
                echo "Modifier group---------------------" . $modifierGroupId . " - " . $modifierGroup->name . " - " . $modifierGroup->id . "\n";
                if (!empty($modifierGroup->id)) {
                    //modifiers
                    $modifiers = $CloverAPI->readModifier($merchant, $modifierGroup->id);
                    if (isset($modifiers->elements)) {
                        foreach ($modifiers->elements as $modifier) {
                            $sql = "SELECT * FROM tbl_clover_modifiers WHERE store_id='" . $merchant['store_id'] . "' AND modifier_group_id='" . $modifierGroupRefId . "' AND clover_id ='" . $modifier->id . "' ";
                            $hasModifier = $db->fetchResult($sql);
                            if (empty($hasModifier)) {
                                $sql = "INSERT INTO `tbl_clover_modifiers` (`id`, `store_id`, `modifier_group_id`, `title`, `clover_id`, `price`, `product_ref_id`) VALUES (NULL, '" . $merchant['store_id'] . "', '$modifierGroupRefId', '" . addslashes($modifier->name) . "', '" . $modifier->id . "', '" . $modifier->price . "','')";
                                $db->executeQuery($sql);
                                $modifierId = $db->lastInsetedId();
                            } else {
                                $modifierId = $hasModifier[0]['id'];
                            }
                            echo "Modifiers--------------------------------" . $modifierId . " - " . $modifier->name . " - " . $modifier->id . "\n";
                            $sql = "UPDATE `tbl_clover_modifier_group` SET price ='" . $modifier->price . "' WHERE id =$modifierGroupRefId";
                            $db->executeQuery($sql);
                        }
                    }
                }
            }
        }
        $attributeLists = [];
        $itemGroupId = 'NULL';
        if (isset($product->itemGroup)) {
            $itemGroup = $CloverAPI->readItemGroup($product->itemGroup->id);
            $sql = "SELECT * FROM tbl_clover_products_groups WHERE store_id ='" . $merchant['store_id'] . "' AND clover_id='" . (isset($itemGroup->id) ? $itemGroup->id : '') . "'";
            $hasItemGroup = $db->fetchResult($sql);
            if (empty($hasItemGroup)) {
                $sql = "INSERT INTO `tbl_clover_products_groups` (`id`, `store_id`, `product_ref_id`, `name`,`clover_id`,`location_id`) VALUES(NULL, '" . $merchant['store_id'] . "', '" . $productId . "', '" . (isset($itemGroup->name) ? $itemGroup->name : null) . "',  '" . (isset($itemGroup->id) ? $itemGroup->id : '') . "',$locationId)";
                $db->executeQuery($sql);
                $itemGroupId = $db->lastInsetedId();
            } else {
                $itemGroupId = $hasItemGroup[0]['id'];
            }
            echo "Item group-----------------------------" . $itemGroupId . " - " . (isset($itemGroup->name) ? $itemGroup->name : null) . " - " . (isset($itemGroup->id) ? $itemGroup->id : '') . "\n";
        }
        if (isset($product->options) && !empty($product->options->elements)) {
            foreach ($product->options->elements as $key => $attribute) {
                if (!isset($attribute->attribute))
                    continue;

                $sql = "SELECT * FROM tbl_clover_products_attributes WHERE product_ref_id = '$productId' AND store_id = '" . $merchant['store_id'] . "' AND clover_id = '" . (isset($attribute->attribute->id) ? $attribute->attribute->id : '') . "'";
                $hasAttribute = $db->fetchResult($sql);
                $attributeId = $attributeName = "";
                if (empty($hasAttribute)) {
                    $responseAttribute = $CloverAPI->readAttributesById($merchant, $attribute->attribute->id);
                    if (isset($responseAttribute->id)) {
                        $sql = "INSERT INTO `tbl_clover_products_attributes` (`id`, `product_ref_id`,  `name`, `clover_id`, `store_id`, `location_id`, `item_group_id`) VALUES(NULL, '" . $productId . "', '" . (isset($responseAttribute->name) ? $responseAttribute->name : '') . "', '" . (isset($responseAttribute->id) ? $responseAttribute->id : '') . "','" . $merchant['store_id'] . "', $locationId, '" . $itemGroupId . "')";
                        $db->executeQuery($sql);
                        $attributeId = $db->lastInsetedId();
                        $attributeName = $attribute->name;
                    }
                } else {
                    $attributeId = $hasAttribute[0]['id'];
                    $attributeName = $hasAttribute[0]['name'];
                }
                $attributeLists[$attribute->attribute->id] = $attributeId;
                echo "Attribute-------------------------------------------- " . $attributeId . " - " . $attributeName . "\n";
            }
        } else {
            if (empty($productRec)) {
                $sql = "UPDATE  tbl_clover_products SET product_ref = " . hexdec(uniqid()) . " WHERE clover_auth_id = '" . $merchant['id'] . "' AND id = '" . $productId . "'";
                $db->executeQuery($sql);
            }
        }
        if (isset($product->options)) {
            foreach ($product->options->elements as $option) {
                $attributeId = $attributeLists[$option->attribute->id];
                $sql = "SELECT * FROM tbl_clover_products_options WHERE attribute_id = '" . $attributeId . "' AND store_id = '" . $merchant['store_id'] . "' AND location_id = $locationId AND clover_id = '" . (isset($option->id) ? $option->id : '') . "'";
                $hasOption = $db->fetchResult($sql);
                if (empty($hasOption)) {
                    $sql = "INSERT INTO `tbl_clover_products_options` (`id`, `product_ref_id`,  `name`, `clover_id`, `store_id`, `attribute_id`, `location_id`) VALUES(NULL, '" . $productId . "', '" . (isset($option->name) ? $option->name : '') . "', '" . (isset($option->id) ? $option->id : '') . "','" . $merchant['store_id'] . "',$attributeId,$locationId)";
                    $db->executeQuery($sql);
                    $optionId = $db->lastInsetedId();
                } else {
                    $optionId = $hasOption[0]['id'];
                }
                echo "Options--------------------------------------------------------" . $optionId . " - " . (isset($option->name) ? $option->name : '') . " - " . (isset($option->id) ? $option->id : '') . "\n";
            }
        }
        if (empty($productRec)) {
            $sql = "UPDATE tbl_clover_products set modifier_group_id=$modifierGroupId,item_group_id=$itemGroupId WHERE id = $productId";
            $db->executeQuery($sql);
        }
    }
    //update product ref for products
    $sql = "SELECT * FROM tbl_clover_products where product_ref IS NULL AND clover_auth_id = '" . $merchant['id'] . "' AND item_group_id IS NOT NULL  GROUP BY item_group_id,clover_auth_id,store_id,location_id";
    $downloadedProducts = $db->fetchResult($sql);
    foreach ($downloadedProducts as $key=> $downloadedProduct){
        $product_ref = hexdec(uniqid());
        $sql = "UPDATE  tbl_clover_products SET product_ref = $product_ref WHERE clover_auth_id = '" . $merchant['id'] . "' AND item_group_id = '" . $downloadedProduct['item_group_id'] . "'";
        $db->executeQuery($sql);
    }
    $sql = "SELECT * FROM tbl_clover_products WHERE in_clover = 1 AND clover_auth_id = '" . $merchant['id'] . "' AND product_ref IS NOT NULL AND in_w3bstore = 0 group by  store_id,location_id,product_ref,clover_auth_id order by id desc";
    $products = $db->fetchResult($sql);
    $productListToW3bstore = [];
    foreach ($products as $product) {
        $modifiersArray = [];
        $sql = "SELECT * FROM `tbl_clover_modifier_group` WHERE ref_id = '" . $product['modifier_group_id'] . "'";
        $modifierGroups = $db->fetchResult($sql);
        $modiferLists = [];
        if (!empty($modifierGroups)) {
            foreach ($modifierGroups as $modifierGroup) {
                $sql = "SELECT * FROM tbl_clover_modifiers WHERE modifier_group_id = '" . $modifierGroup['id'] . "'";
                $modifiers = $db->fetchResult($sql);
                $modifiersVariations = [];
                foreach ($modifiers as $modifier) {
                    $modifiersVariations[] = [
                        'name' => (!empty($modifier['title']) ? $modifier['title'] : "N/A"),
                        'location_name' => $locationName,
                        "weight" => "",
                        "height" => "",
                        "length" => "",
                        "width" => "",
                        "barcode" => $product['code'],
                        "reorder_point" => "",
                        "item_details" => [
                            "lot_remaining" => (!empty($stock) ? $stock[0]['stock_count'] : 0),
                            "cogs_per_unit" => "",
                            "offer_price" => ($modifier['price'] / 100),
                            "commission" => "",
//                        "wholesale_price" => $modifier['price'],
                            "unit_cost" => ($product['cost'] == 0 ? "" : $product['cost'] / 100),
//                            "list_price" => ($productList['price'] / 100),
                            "warehouse_bin" => "",
                            "low_threshold" => "",
                        ],
                    ];
                }
                $modiferLists[] = [
                    "Legacy SKU" => "",
                    "Product Title" => $modifierGroup['title'],
                    "Product Category" => [
                        [
                            "name" => "",
                        ]
                    ],
                    "Variants" => $modifiersVariations,
                    "Tax - exempt" => "",
                    "Make" => "",
                    "Model" => "",
                    "Summary Description" => "",
                    "Detailed Description" => "",
                    "Ship to customer" => 1,
                    "Pickup in store" => 1,
                    "Charge by Weight" => "",
                    "Accessories Title" => "",
                    "Accessories Limit" => "",
                    "Accessories Position" => "",
                ];
            }
        }
        $variations = [];
        $attributeId = [];
        $sql = "SELECT * FROM tbl_clover_products WHERE in_clover = 1 AND member_item_id IS NULL AND in_w3bstore = 0 AND store_id = '" . $product['store_id'] . "' AND location_id = $locationId AND product_ref = '" . $product['product_ref'] . "'";
        $productLists = $db->fetchResult($sql);
        $itemGroupName = "";
        foreach ($productLists as $productList) {
            $stock = $db->fetchResult("SELECT * FROM tbl_clover_products_stock WHERE item_id = '" . $productList['id'] . "'");
            $attributes = $db->fetchResult("SELECT * FROM tbl_clover_products_attributes WHERE product_ref_id = '" . $productList['id'] . "' AND store_id = '" . $product['store_id'] . "' ");
            $sql = "SELECT * FROM tbl_clover_products_groups WHERE id = '" . $productList['item_group_id'] . "'";
            $itemGroup = $db->fetchResult($sql);
            $optionNames = [];
            $attributeLists = [];
            if (!empty($itemGroup)) {
                $itemGroupName = $itemGroup[0]['name'];
                $variationLists = [];
                foreach ($attributes as $attribute) {
                    $attributeLists[] = ['product_option_name' => $attribute['name']];
                    $options = $db->fetchResult("SELECT * FROM tbl_clover_products_options WHERE attribute_id = '" . $attribute['id'] . "' ORDER BY id ASC LIMIT 1");
                    if (empty($options))
                        continue;
                    $optionNames[] = $options[0]['name'];
                }
            }
            $variations[] = [
                'name' => (!empty($optionNames) ? implode(",", $optionNames) : "N/A"),
                'location_name' => $locationName,
                "weight" => "",
                "height" => "",
                "length" => "",
                "width" => "",
                "barcode" => $productList['code'],
                "reorder_point" => "",
                "item_details" => [
                    'identifier' => $productList['id'],
                    "lot_remaining" => (!empty($stock) ? $stock[0]['stock_count'] : 0),
                    "cogs_per_unit" => "",
                    "offer_price" => ($productList['price'] / 100),
                    "commission" => "",
                    "wholesale_price" => ($productList['price'] / 100),
                    "unit_cost" => ($productList['cost'] == 0 ? "" : $productList['cost'] / 100),
//                            "list_price" => ($productList['price'] / 100),
                    "warehouse_bin" => "",
                    "low_threshold" => "",
                ],
            ];
        }
        $productListToW3bstore[$product['clover_auth_id']][$product['store_id']][$product['product_ref']][] = [
            "Legacy SKU" => "",
            "Product Title" => (!empty($itemGroupName) ? $itemGroupName : $product['name']),
            "Product Category" => [
                [
                    "name" => "",
                ]
            ],
            "Tax - exempt" => "",
            "Make" => "",
            "Model" => "",
            "Summary Description" => "",
            "Detailed Description" => "",
            "Options" => $attributeLists,
            "Variants" => $variations,
            "Ship to customer" => 1,
            "Pickup in store" => 1,
            "Charge by Weight" => "",
            "Accessories Title" => "",
            "Accessories Limit" => "",
            "Accessories Position" => "",
            "Accessory" => $modiferLists
        ];
    }
    foreach ($productListToW3bstore as $merchantAuthId => $products) {
        $merchantId = $merchant['w3b_merchant_id'];
        $apiKey = $merchant['w3b_api_key'];
        foreach ($products as $storeId => $stores) {
            $w3bstore = new W3bStore\W3bstoreAPI($storeId, $merchantId, $apiKey);
            foreach ($stores as $productRef => $storeProducts) {
                echo "Product reference - " . $productRef . "\n";
                foreach ($storeProducts as $product) {
                    $requestBody = json_encode(array($product));
                    $response = $w3bstore->request($ENV['W3BSTORE_API'], 'POST', $requestBody, 'add_items');
                    if ($response['error'] == 1) {
                        echo(isset($response['msg']) ? $response['msg'] . "\n" : "\n");
                        $w3bstore->logApiError($merchant['id'], 'w3bstore-add_items', $requestBody, $response, (isset($response['msg']) ? $response['msg'] : ""));
                        continue;
                    }
                    $response = $response['response'];
                    if ($response['result'] == 'OK' || $response['result'] == 'ok') {
                        $listedProducts = $response['ids'];
                        foreach ($listedProducts as $listedProduct) {
                            $parentProductId = $listedProduct['id'];
                            echo "W3bstore product listed - " . $parentProductId . "\n";
                            foreach ($listedProduct['item_details_ids'] as $item_details_id) {
                                echo "-------------------Item list id - " . $item_details_id['id'] . "\n";
                                $db->executeQuery("UPDATE tbl_clover_products SET member_item_id = $parentProductId, item_detail_id = '" . $item_details_id['id'] . "' WHERE id = '" . $item_details_id['identifier'] . "'");
                            }
                            $db->executeQuery("UPDATE tbl_clover_products SET member_item_id = $parentProductId, in_w3bstore = 1 WHERE product_ref = $productRef");
                        }
                    } else {
                        echo "W3bstore API error - " . $response['msg'] . "\n";
                        $w3bstore->logApiError($merchant['id'], 'w3bstore-add_items', $requestBody, $response, (isset($response['msg']) ? $response['msg'] : ""));
                        continue;
                    }
                    echo "\n\n";
                }
            }
        }
    }
}
echo "\n\nDone!";
