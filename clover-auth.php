<?php
/**
 * Created by PhpStorm.
 * User: Salih Mohamed
 * Date: 8/15/2019
 * Time: 11:47 PM
 */
session_start();
if (!isset($_SESSION['user'])) {
    header("Location: login.php");
    exit;
}
include __DIR__ . "/db/Database.php";
$db = new Database();
$hasRec = $db->fetchResult("SELECT  * FROM tbl_clover_authentications WHERE user_id ='" . $_SESSION['user']['id'] . "'");
if (empty($hasRec)) {
    header("Location: oauth.php");
    exit;
}
header("Location: oauth-success.php");
exit;
