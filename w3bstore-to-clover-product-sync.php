<?php
/**
 * Created by PhpStorm.
 * User: Salih Mohamed
 * Date: 12/10/2019
 * Time: 7:35 PM
 */
set_time_limit(0);
include __DIR__ . "/db/Database.php";
$db = new Database();
include './CloverAPI.php';
include './W3bstoreAPI.php';
include './vendor/autoload.php';
if (file_exists('./env.php')) {
    include './env.php';
}
global $ENV;
$CloverAPI = new W3bStore\CloverAPI();
$merchants = $db->fetchResult("SELECT * from tbl_clover_authentications WHERE location_id IS NOT NULL AND store_id IS NOT NULL AND token IS NOT NULL AND w3b_merchant_id IS NOT NULL AND merchant_id IS NOT NULL AND w3b_api_key IS NOT NULL");
foreach ($merchants as $m_key => $merchant) {
    $store_id = $merchant['store_id'];
    $location_id = $merchant['location_id'];
    $W3bstoreAPI = new W3bStore\W3bstoreAPI($merchant['store_id'], $merchant['w3b_merchant_id'], $merchant['w3b_api_key']);
    $response = $W3bstoreAPI->request($ENV['W3BSTORE_API'], 'GET', $requestBody = null, 'get_items', $parameters = null);
    if ($response['error'] == 0) {
        $products = $response['response'];
        $member_store_items_array = [];
        $minMaxPrice = [];
        if (empty($products))
            continue;
        foreach ($products as $product) {
            $unauthorized = 0;
            $itemNumber = $product['Item Number'];
            $lastModifierGroupId = null;
            $modifierGroupIds = [];
            $modifierGroupNames = [];
            if (!empty($product['Accessory'])) {
                foreach ($product['Accessory'] as $accessory) {
                    $modifierGroupTitle = trim($accessory['Product Title']);
                    $accessoryVariations = [];
                    $has_modifier_group = $db->fetchResult('SELECT * FROM tbl_clover_modifier_group WHERE item_id ="' . $itemNumber . '" AND title ="' . addslashes($modifierGroupTitle) . '" AND store_id="' . $store_id . '" AND location_id="' . $location_id . '"');
                    $clover_modifier_group_id = "";
                    if (empty($has_modifier_group) && !empty($accessory['Product Title'])) {
                        foreach ($accessory['Variants'] as $variant) {
                            if ($variant['location_id'] != $merchant['location_id'])
                                continue;
                            $data = ['name' => $accessory['Product Title'], 'maxAllowed' => $variant['item_details']['offer_price']];
                            $response = $CloverAPI->createModifierGroup($data);
                            if (!empty($response['id'])) {
                                $clover_modifier_group_id = $response['id'];
                                $sql = "INSERT INTO `tbl_clover_modifier_group` (`id`, `store_id`, `title`,`clover_id`,`item_id`,`location_id`) VALUES (NULL, '$store_id', '" . addslashes($modifierGroupTitle) . "', '" . $clover_modifier_group_id . "','" . $itemNumber . "','" . $location_id . "')";
                                $db->executeQuery($sql);
                                $lastModifierGroupId = $db->lastInsetedId();
                            } else {
                                $W3bstoreAPI->logApiError($merchant['id'], 'clover-add_modifier', $data, $response, (isset($response['message']) ? $response['message'] : ''));
                                if (isset($response['message']) && $response['message'] == '401 Unauthorized') {
                                    $unauthorized = 1;
                                    continue;
                                }
                            }
                            continue;
                        }
                    } else {
                        $clover_modifier_group_id = $has_modifier_group[0]['clover_id'];
                        $lastModifierGroupId = $has_modifier_group[0]['id'];
                    }
                    $modifierGroupIds[] = ['id' => $clover_modifier_group_id];
                    $modifierGroupNames[] = $modifierGroupTitle;
                    echo "Modifier group----------------------" . $modifierGroupTitle . " - " . $clover_modifier_group_id . "\n";
                    foreach ($accessory['Variants'] as $key => $accessoryVariation) {
                        if ($accessoryVariation['location_id'] != $merchant['location_id'])
                            continue;
                        if (!empty($clover_modifier_group_id)) {
                            $modifierTitle = $accessoryVariation['name'];
                            if (!empty($modifierTitle)) {
                                $modifierId = $accessoryVariation['id'];
                                $price = $accessoryVariation['item_details']['offer_price'] * 100;
                                $minMaxPrice[] = $price;
                                $sql = "SELECT * FROM tbl_clover_modifiers WHERE store_id='" . $store_id . "' AND location_id='" . $location_id . "' AND modifier_group_id='" . $lastModifierGroupId . "' AND item_id='" . $itemNumber . "'AND modifier_id='" . $modifierId . "'";
                                $hasRec = $db->fetchResult($sql);
                                $clover_modifier_id = "";
                                if (empty($hasRec)) {
                                    $response = $CloverAPI->createModifier($clover_modifier_group_id, $modifierTitle, $price);
                                    if (!empty($response['id'])) {
                                        $clover_modifier_id = $response['id'];
                                        $sql = "INSERT INTO `tbl_clover_modifiers` (`id`, `store_id`,`modifier_group_id`,`clover_id`,`item_id`,`title`,`price`,`modifier_id`,`location_id`) VALUES(NULL, '$store_id', '" . $lastModifierGroupId . "','" . $clover_modifier_id . "','$itemNumber','" . addslashes($modifierTitle) . "','" . $price . "','" . $modifierId . "','" . $location_id . "')";
                                        $db->executeQuery($sql);
                                    } else {
                                        $W3bstoreAPI->logApiError($merchant['id'], 'clover-add_modifier', ['clover_id' => $clover_modifier_group_id, 'name' => $modifierTitle, 'price' => (int)$price], $response, (isset($response['message']) ? $response['message'] : ''));
                                        if (isset($response['message']) && $response['message'] == '401 Unauthorized') {
                                            $unauthorized = 1;
                                            continue;
                                        }
                                    }
                                } else {
                                    $clover_modifier_id = $hasRec[0]['clover_id'];
                                }
                                echo "Modifiers----------------------------------------" . $modifierTitle . " - " . $clover_modifier_id . "\n";
                            }
                        }
                    }
                }
            }
            if (!empty($product['Options'])) {
                $group_name = strtolower($product['Product Title']);
                $has_group_name = $db->fetchResult('SELECT * FROM tbl_clover_products_groups WHERE item_id ="' . $itemNumber . '" AND store_id="' . $store_id . '" AND location_id="' . $location_id . '"');
                $clover_group_id = "";
                if (empty($has_group_name)) {
                    //create in clover
                    $response = $CloverAPI->createNewGroup($group_name);
                    if (!empty($response['id'])) {
                        $clover_group_id = $response['id'];
                        $sql = "INSERT INTO `tbl_clover_products_groups` (`id`, `store_id`, `item_id`, `clover_id`, `name`, `location_id`) VALUES (NULL, '$store_id', '$itemNumber', '$clover_group_id', '" . addslashes($group_name) . "', '" . $location_id . "')";
                        $db->executeQuery($sql);
                        $lastGroupId = $db->lastInsetedId();
                    } else {
                        $W3bstoreAPI->logApiError($merchant['id'], 'clover-add_item_group', ['name' => $group_name], $response, (isset($response['message']) ? $response['message'] : ''));
                        if (isset($response['message']) && $response['message'] == '401 Unauthorized') {
                            $unauthorized = 1;
                            continue;
                        }
                    }
                } else {
                    $clover_group_id = $has_group_name[0]['clover_id'];
                    $lastGroupId = $has_group_name[0]['id'];
                }
                echo "Item Group -----------------" . $group_name . " - " . $clover_group_id . "\n";
                $attributeLists = [];
                $attributeIdLists = [];
                foreach ($product['Options'] as $option) {
                    $attributeId = $option['id'];
                    $attributeName = trim($option['product_option_name']);
                    if (empty($attributeName))
                        continue;
                    $has_attribute = $db->fetchResult('SELECT * FROM tbl_clover_products_attributes WHERE item_id ="' . $itemNumber . '" AND attribute_id ="' . $attributeId . '" AND store_id="' . $store_id . '" AND location_id="' . $location_id . '"');
                    $clover_attribute_id = $lastAttributeId = "";
                    if (empty($has_attribute)) {
                        $response = $CloverAPI->createNewAtributes($attributeName, $clover_group_id);
                        if (!empty($response['id'])) {
                            $clover_attribute_id = $response['id'];
                            $sql = "INSERT INTO `tbl_clover_products_attributes` (`id`, `store_id`, `item_id`, `clover_id`, `name`, `item_group_id`, `attribute_id`, `location_id`) VALUES (NULL, '$store_id', '$itemNumber', '$clover_attribute_id', '" . addslashes($attributeName) . "', '$lastGroupId','$attributeId','$location_id')";
                            $db->executeQuery($sql);
                            $lastAttributeId = $db->lastInsetedId();
                        } else {
                            $W3bstoreAPI->logApiError($merchant['id'], 'clover-add_attribute', ['clover_id' => $clover_group_id, 'name' => $attributeName], $response, (isset($response['message']) ? $response['message'] : ''));
                            if (isset($response['message']) && $response['message'] == '401 Unauthorized') {
                                $unauthorized = 1;
                                continue;
                            }
                        }
                    } else {
                        $lastAttributeId = $has_attribute[0]['id'];
                        $clover_attribute_id = $has_attribute[0]['clover_id'];
                        $attributeName = $has_attribute[0]['name'];
                    }
                    $attributeLists[] = $clover_attribute_id;
                    $attributeIdLists[$clover_attribute_id] = $lastAttributeId;
                    echo "Attributes--------------------------------------------------------------------------" . $attributeName . " - " . $clover_attribute_id . "\n\n";
                }
                foreach ($product['Variants'] as $productVariants) {
                    if ($productVariants['location_id'] != $merchant['location_id'])
                        continue;
                    $item_detail = $productVariants['item_details'];
                    $has_product = $db->fetchResult('SELECT * FROM tbl_clover_products WHERE  member_item_id = "' . $itemNumber . '" AND store_id="' . $store_id . '" AND location_id="' . $location_id . '" AND item_detail_id="' . $item_detail['id'] . '"');
                    $clover_product_id = null;

                    if (empty($item_detail)) {
                        $W3bstoreAPI->logApiError($merchant['id'], 'clover-add_product', '', '', 'W3bstore ' . $item_detail['id'] . ' item details not found');
                        continue;
                    }
                    if (empty($product['Product Title'])) {
                        $W3bstoreAPI->logApiError($merchant['id'], 'clover-add_product', '', '', 'W3bstore ' . $item_detail['id'] . ' product title not found');
                        continue;
                    }
                    if (empty($has_product)) {
                        $sku = $item_detail['id'];
                        $barcode = $productVariants['barcode'];
                        $new_item = [
                            'name' => $product['Product Title'],
                            'isRevenue' => 1,
                            'code' => $barcode,
                            'sku' => $sku,
                            'price' => $item_detail['offer_price'] * 100,
                            'priceType' => 'FIXED',
                            'cost' => $item_detail['cogs_per_unit'] * 100,
                            'isRevenue' => true,
                        ];
                        if (!empty($clover_group_id)) {
                            $new_item['itemGroup'] = [
                                'id' => $clover_group_id,
                            ];
                        }
                        if (!empty($clover_modifier_group_id)) {
                            $new_item['modifierGroups'] = $modifierGroupIds;
                        }
                        $response = $CloverAPI->createNewProduct($new_item);
                        if (!empty($response['id'])) {
                            $clover_product_id = $response['id'];
                            $sql = "INSERT INTO `tbl_clover_products` (`id`, `store_id`, `location_id`, `mkp_item_id`,`member_item_id`,`item_detail_id`, `clover_id`, `in_w3bstore`,`in_clover`, `item_group_id` , `clover_auth_id`, `name`, `sku`, `unit_name`,`cost`,`code`,`price`,`price_type`,`default_tax_rate`,`is_revenue`,`modifier_group_id`,`status`) VALUES(NULL, '$store_id','$location_id', null, '" . $itemNumber . "','" . $item_detail['id'] . "', '$clover_product_id',1,1,'$lastGroupId','" . $merchant['id'] . "','" . addslashes($product['Product Title']) . "','" . $sku . "','','" . ($item_detail['cogs_per_unit'] * 100) . "','" . $barcode . "','" . ($item_detail['offer_price'] * 100) . "','FIXED','1','1','$lastModifierGroupId','UPDATE PRICES FALSE')";
                            $db->executeQuery($sql);
                            $lastProductId = $db->lastInsetedId();
                        } else {
                            $W3bstoreAPI->logApiError($merchant['id'], 'clover-add_product', $new_item, $response, (isset($response['message']) ? $response['message'] : ''));
                            if (isset($response['message']) && $response['message'] == '401 Unauthorized') {
                                $unauthorized = 1;
                                continue;
                            }
                        }
                    } else {
                        $item_detail_price = $item_detail['offer_price'] * 100;
                        if (trim($item_detail_price) == trim($has_product[0]['price'])) {
                        } else {
                            $clover_product_id = $has_product[0]['clover_id'];
                            $lastProductId = $has_product[0]['id'];
                            $sql = "UPDATE tbl_clover_products SET status ='UPDATE CLOVER PRICES TRUE', price='" . trim($item_detail['offer_price'] * 100) . "' WHERE id = $lastProductId";
                            $db->executeQuery($sql);
                            $price_update = true;
                        }
                    }
                    if (!empty($lastProductId)) {
                        $hasStock = $db->fetchResult("SELECT * FROM tbl_clover_products_stock WHERE item_id = $lastProductId");
                        if (empty($hasStock)) {
                            $stock = $item_detail['lot_remaining'];
                            $response = $CloverAPI->updateStock($clover_product_id, $stock);
                            if (empty($response['modifiedTime'])) {
                                //error
                                $W3bstoreAPI->logApiError($merchant['id'], 'clover-add_stock', ['clover_id' => $clover_product_id, 'stock' => $stock], $response, (isset($response['message']) ? $response['message'] : ''));
                            } else {
                                $sql = "INSERT INTO `tbl_clover_products_stock` (`id`, `item_id`, `stock_count`, `quantity`,`status`) VALUES(NULL, '$lastProductId', '" . $item_detail['lot_remaining'] . "', '" . $item_detail['lot_remaining'] . "','UPDATE STOCK FALSE')";
                                $db->executeQuery($sql);
                                $lastStockId = $db->lastInsetedId();
                            }
                        } else {
                            $lastStockId = $hasStock[0]['id'];
                            if ($hasStock[0]['quantity'] != $item_detail['lot_remaining']) {
                                $sql = "UPDATE `tbl_clover_products_stock` SET status ='UPDATE STOCK TRUE', quantity='" . $item_detail['lot_remaining'] . "' , stock_count='" . $item_detail['lot_remaining'] . "'  WHERE id='$lastStockId'";
                                $db->executeQuery($sql);
                            }
                        }
                        echo "Product-------------------------------------------------------------------------------------------------------" . trim($product['Product Title']) . " - " . $clover_product_id . "\n";
                    $variationNames = $productVariants['name'];
                    if (empty($variationNames))
                        continue;
                    $variationNames = explode(',', $productVariants['name']);
                    foreach ($variationNames as $key => $variationName) {
                        $clover_attribute_id = $attributeLists[$key];
                        $lastAttributeId = $attributeIdLists[$clover_attribute_id];
                        $has_options = $db->fetchResult('SELECT * FROM tbl_clover_products_options WHERE name="' . $variationName . '" AND   item_id ="' . $itemNumber . '" AND   product_ref_id ="' . $lastProductId . '" ');
                        $clover_variation_id = null;
                        if (empty($has_options)) {
                            $response = $CloverAPI->createNewOptions($variationName, $clover_attribute_id);
                            if (!empty($response['id'])) {
                                $clover_variation_id = $response['id'];
                                $sql = "INSERT INTO `tbl_clover_products_options` (`id`, `store_id`,`location_id`, `item_id`, `attribute_id`, `clover_id`, `name`, `product_ref_id`) VALUES (NULL, '$store_id', '$location_id', '$itemNumber', '$lastAttributeId', '$clover_variation_id', '" . addslashes($variationName) . "', '" . $lastProductId . "')";
                                $db->executeQuery($sql);
                                $lastOptionId = $db->lastInsetedId();
                            } else {
                                $W3bstoreAPI->logApiError($merchant['id'], 'clover-add_options', ['name' => $variationName, 'clover_id' => $clover_attribute_id], $response, (isset($response['message']) ? $response['message'] : ''));
                                if (isset($response['message']) && $response['message'] == '401 Unauthorized') {
                                    $unauthorized = 1;
                                    continue;
                                }
                            }
                        } else {
                            $clover_variation_id = $has_options[0]['clover_id'];
                            $lastOptionId = $has_options[0]['id'];
                            $optionName = $has_options[0]['name'];
                        }
                        echo "Options-----------------------------------------------------------------------------------------------------------" . $variationName . " - " . $clover_variation_id . "\n";
                        $response = $CloverAPI->createNewOptionItems($clover_product_id, $clover_variation_id);
                    }
                    }
                    echo "\n";
                }
            } else {
                foreach ($product['Variants'] as $productVariants) {
                    if ($productVariants['location_id'] != $merchant['location_id'])
                        continue;
                    $item_detail = $productVariants['item_details'];
                    $has_product = $db->fetchResult('SELECT * FROM tbl_clover_products WHERE  member_item_id = "' . $itemNumber . '" AND store_id="' . $store_id . '" AND location_id="' . $location_id . '" AND item_detail_id="' . $item_detail['id'] . '"');
                    $clover_product_id = null;


                    if (empty($item_detail)) {
                        $W3bstoreAPI->logApiError($merchant['id'], 'clover-add_product', '', '', 'W3bstore ' . $item_detail['id'] . ' item details not found');
                        continue;
                    }
                    if (empty($product['Product Title'])) {
                        $W3bstoreAPI->logApiError($merchant['id'], 'clover-add_product', '', '', 'W3bstore ' . $item_detail['id'] . ' product title not found');
                        continue;
                    }

                    if (empty($has_product)) {
                        $sku = $item_detail['id'];
                        $barcode = $productVariants['barcode'];
                        $new_item = [
                            'name' => $product['Product Title'],
                            'isRevenue' => 1,
                            'code' => $barcode,
                            'sku' => $sku,
                            'price' => $item_detail['offer_price'] * 100,
                            'priceType' => 'FIXED',
                            'cost' => $item_detail['cogs_per_unit'] * 100,
                            'isRevenue' => true,
                        ];
                        if (!empty($clover_modifier_group_id)) {
                            $new_item['modifierGroups'] = $modifierGroupIds;
                        }
                        $response = $CloverAPI->createNewProduct($new_item);
                        if (!empty($response['id'])) {
                            $clover_product_id = $response['id'];
                            $sql = "INSERT INTO `tbl_clover_products` (`id`, `store_id`,`location_id`, `mkp_item_id`,`member_item_id`,`item_detail_id`, `clover_id`, `in_w3bstore`,`in_clover`,  `clover_auth_id`, `name`, `sku`, `unit_name`,`cost`,`code`,`price`,`price_type`,`default_tax_rate`,`is_revenue`,`modifier_group_id`,`status`) VALUES(NULL, '$store_id','$location_id', null, '" . $itemNumber . "','" . $item_detail['id'] . "', '$clover_product_id',1,1,'" . $merchant['id'] . "','" . addslashes($product['Product Title']) . "','" . $sku . "','','" . ($item_detail['cogs_per_unit'] * 100) . "','" . $barcode . "','" . ($item_detail['offer_price'] * 100) . "','FIXED','1','1','$lastModifierGroupId','UPDATE PRICES FALSE')";
                            $db->executeQuery($sql);
                            $lastProductId = $db->lastInsetedId();
                        } else {
                            $W3bstoreAPI->logApiError($merchant['id'], 'clover-add_product', $new_item, $response, (isset($response['message']) ? $response['message'] : ''));
                            if (isset($response['message']) && $response['message'] == '401 Unauthorized') {
                                $unauthorized = 1;
                                continue;
                            }
                        }
                    } else {
                        $lastProductId = $has_product[0]['id'];
                        $item_detail_price = $item_detail['offer_price'] * 100;
                        if (trim($item_detail_price) == trim($has_product[0]['price'])) {
                        } else {
                            $clover_product_id = $has_product[0]['clover_id'];
                            $sql = "UPDATE tbl_clover_products SET status ='UPDATE CLOVER PRICES TRUE', price='" . trim($item_detail['offer_price'] * 100) . "' WHERE id = $lastProductId";
                            $db->executeQuery($sql);
                            $price_update = true;
                        }
                    }
                    if (!empty($lastProductId)) {
                        $hasStock = $db->fetchResult("SELECT * FROM tbl_clover_products_stock WHERE item_id = $lastProductId");
                        if (empty($hasStock)) {
                            $stock = $item_detail['lot_remaining'];
                            $response = $CloverAPI->updateStock($clover_product_id, $stock);
                            if (empty($response['modifiedTime'])) {
                                //error
                                $W3bstoreAPI->logApiError($merchant['id'], 'clover-update_stock', ['clover_id' => $clover_product_id, 'qty' => $stock], $response, (isset($response['message']) ? $response['message'] : ''));
                            } else {
                                $sql = "INSERT INTO `tbl_clover_products_stock` (`id`, `item_id`, `stock_count`, `quantity`,`status`) VALUES(NULL, '$lastProductId', '" . $item_detail['lot_remaining'] . "', '" . $item_detail['lot_remaining'] . "','UPDATE STOCK FALSE')";
                                $db->executeQuery($sql);
                                $lastStockId = $db->lastInsetedId();
                            }
                        } else {
                            $lastStockId = $hasStock[0]['id'];
                            if (trim($hasStock[0]['quantity']) != trim($item_detail['lot_remaining'])) {
                                $sql = "UPDATE `tbl_clover_products_stock` SET stock_count='" . $item_detail['lot_remaining'] . "',quantity='" . $item_detail['lot_remaining'] . "', status ='UPDATE CLOVER STOCK TRUE' WHERE id='" . $lastStockId . "'";
                                $db->executeQuery($sql);
                            }
                        }
                    }
                    echo "Product-------------------------------------------------------------------------------------------------------" . trim($product['Product Title']) . " - " . $clover_product_id . "\n";
                    continue;
                }
            }

            if ($unauthorized == 1) {
                break;
            }
            sleep(2);
        }
    } else {
        echo $response['msg'] . "\n";
        $W3bstoreAPI->logApiError($merchant['id'], 'w3bstore-get_items', '', $response, (isset($response['msg']) ? $response['msg'] : ''));
    }
}
echo "\nDone\n";

