<?php
/**
 * Created by PhpStorm.
 * User: Salih Mohamed
 * Date: 2/3/2020
 * Time: 1:59 AM
 */
set_time_limit(0);
include __DIR__ . "/db/Database.php";
include __DIR__ . '/CloverAPI.php';
include __DIR__ . '/W3bstoreAPI.php';
include __DIR__ . '/vendor/autoload.php';
if (file_exists('./env.php')) {
    include './env.php';
}
global $ENV;
$db = new Database();
$CloverAPI = new \W3bStore\CloverAPI();
//update price for w3bstore
$merchants = $db->fetchResult("SELECT * from tbl_clover_authentications order by id asc limit 1");
echo "Start w3bstore price updating\n";
foreach ($merchants as $m_key => $merchant) {
    $w3bstore = new W3bStore\W3bstoreAPI($merchant['store_id'], $merchant['w3b_merchant_id'], $merchant['w3b_api_key'], 'setprice/');
    $sql = "SELECT * FROM tbl_clover_products WHERE clover_auth_id = '" . $merchant['id'] . "' AND status = 'UPDATE W3BSTORE PRICES TRUE'";
    $products = $db->fetchResult($sql);
    $w3bstorePriceUpdateProducts = [];
    foreach ($products as $product) {
        $w3bstorePriceUpdateProducts[] = ['item_detail_id' => $product['item_detail_id'], 'price' => ($product['price'] / 100)];
    }
    if (empty($w3bstorePriceUpdateProducts))
        continue;

    $response = $w3bstore->request($ENV['W3BSTORE_API'], 'POST', json_encode($w3bstorePriceUpdateProducts));
    if ($response['error'] == 1) {
        echo "Store id- " . $merchant['store_id'] . "\n";
        echo "Location id - " . $merchant['location_id'] . "\n";
        echo "Merchant id - " . $merchant['w3b_merchant_id'] . "\n";
        echo $response['msg'] . "\n\n";
        $w3bstore->logApiError($merchant['id'], 'w3bstore-setprice', $w3bstorePriceUpdateProducts, $response, $response['msg']);
        continue;
    }
    $syncPrducts = $response['response'];
    foreach ($syncPrducts as $syncPrduct) {
        if ($syncPrduct['result'] == 'OK' || $syncPrduct['result'] == 'ok') {
            echo "Store id- " . $merchant['store_id'] . "\n";
            echo "Location id - " . $merchant['location_id'] . "\n";
            echo "Merchant id - " . $merchant['w3b_merchant_id'] . "\n";
            echo "Item detail id - " . $syncPrduct['item_detail_id'] . " - Updated\n\n";
            $db->executeQuery("UPDATE tbl_clover_products SET status=NULL WHERE item_detail_id = '" . $syncPrduct['item_detail_id'] . "' AND store_id='" . $merchant['store_id'] . "' AND location_id='" . $merchant['location_id'] . "' ");
        } else {
            echo "Store id- " . $merchant['store_id'] . "\n";
            echo "Location id - " . $merchant['location_id'] . "\n";
            echo "Merchant id - " . $merchant['w3b_merchant_id'] . "\n";
            echo "Item detail id - " . $syncPrduct['item_detail_id'] . " - Error on updated\n\n";
            $w3bstore->logApiError($merchant['id'], 'w3bstore-setprice', $w3bstorePriceUpdateProducts, $response, "Item detail id - " . $syncPrduct['item_detail_id'] . " - Error on updated");
        }
    }
}
echo "End w3bstore price\n\n";
echo "Start w3bstore stock updating\n";
foreach ($merchants as $m_key => $merchant) {
    $w3bstore = new W3bStore\W3bstoreAPI($merchant['store_id'], $merchant['w3b_merchant_id'], $merchant['w3b_api_key'], 'setqty/');
    $sql = "SELECT tbl_clover_products.id, tbl_clover_products.item_detail_id, tbl_clover_products_stock.stock_count,tbl_clover_products_stock.id as stock_id FROM tbl_clover_products LEFT JOIN tbl_clover_products_stock on tbl_clover_products.id = tbl_clover_products_stock.item_id WHERE tbl_clover_products.clover_auth_id = '" . $merchant['id'] . "' AND tbl_clover_products_stock.status = 'UPDATE W3BSTORE STOCK TRUE'";
    $products = $db->fetchResult($sql);
    $w3bstoreStockUpdateProducts = [];
    foreach ($products as $product) {
        $w3bstoreStockUpdateProducts[] = ['item_detail_id' => $product['item_detail_id'], 'qty' => $product['stock_count']];
    }
    if (empty($w3bstoreStockUpdateProducts))
        continue;
    $response = $w3bstore->request('POST', json_encode($w3bstoreStockUpdateProducts));
    if ($response['error'] == 1) {
        echo "Store id- " . $merchant['store_id'] . "\n";
        echo "Location id - " . $merchant['location_id'] . "\n";
        echo "Merchant id - " . $merchant['w3b_merchant_id'] . "\n";
        echo $response['msg'] . "\n\n";
        $w3bstore->logApiError($merchant['id'], 'w3bstore-setqty', $w3bstoreStockUpdateProducts, $response, $response['msg']);
        continue;
    }
    $syncPrducts = $response['response'];
    foreach ($syncPrducts as $syncPrduct) {
        if ($syncPrduct['result'] == 'OK' || $syncPrduct['result'] == 'ok') {
            echo "Store id- " . $merchant['store_id'] . "\n";
            echo "Location id - " . $merchant['location_id'] . "\n";
            echo "Merchant id - " . $merchant['w3b_merchant_id'] . "\n";
            echo "Item detail id - " . $syncPrduct['item_detail_id'] . " - Updated\n\n";
            $db->executeQuery("UPDATE tbl_clover_products_stock SET status='UPDATE STOCK FALSE' WHERE id = '" . $syncPrduct['stock_id'] . "'");
        } else {
            echo "Store id- " . $merchant['store_id'] . "\n";
            echo "Location id - " . $merchant['location_id'] . "\n";
            echo "Merchant id - " . $merchant['w3b_merchant_id'] . "\n";
            echo "Item detail id - " . $syncPrduct['item_detail_id'] . " - Error on updated\n\n";
            $w3bstore->logApiError($merchant['id'], 'w3bstore-setqty', $w3bstoreStockUpdateProducts, $response, $response['msg']);
        }
    }
}
echo "End w3bstore stock\n\n";
echo "Start clover price updating\n";
$CloverAPI = new W3bStore\CloverAPI();
foreach ($merchants as $m_key => $merchant) {
    $w3bstore = new W3bStore\W3bstoreAPI($merchant['store_id'], $merchant['w3b_merchant_id'], $merchant['w3b_api_key'], 'setprice/');
    $sql = "SELECT * FROM tbl_clover_products WHERE clover_auth_id = '" . $merchant['id'] . "' AND status = 'UPDATE CLOVER PRICES TRUE'";
    $products = $db->fetchResult($sql);
    $cloverPriceUpdateProducts = [];
    foreach ($products as $product) {
        $response = $CloverAPI->updateProductPrices($product['clover_id'], $product['price'], $product['cost']);
        if (!empty($response['id'])) {
            echo "Store id- " . $merchant['store_id'] . "\n";
            echo "Location id - " . $merchant['location_id'] . "\n";
            echo "Merchant id - " . $merchant['w3b_merchant_id'] . "\n";
            echo "Item detail id - " . $product['item_detail_id'] . " - Updated\n\n";
            $db->executeQuery("UPDATE tbl_clover_products SET status='UPDATE PRICE FALSE' WHERE item_detail_id = '" . $product['item_detail_id'] . "' AND store_id='" . $merchant['store_id'] . "' AND location_id='" . $merchant['location_id'] . "' ");
        } else {
            $w3bstore->logApiError($merchant['id'], 'clover-setprice', $product, $response, '');
        }
    }
}
echo "End clover price\n\n";
echo "Start clover stock updating\n";
foreach ($merchants as $m_key => $merchant) {
    $w3bstore = new W3bStore\W3bstoreAPI($merchant['store_id'], $merchant['w3b_merchant_id'], $merchant['w3b_api_key'], 'setqty/');
    $sql = "SELECT tbl_clover_products.id, tbl_clover_products.item_detail_id,tbl_clover_products.clover_id, tbl_clover_products_stock.stock_count,tbl_clover_products_stock.id as stock_id FROM tbl_clover_products LEFT JOIN tbl_clover_products_stock on tbl_clover_products.id = tbl_clover_products_stock.item_id WHERE tbl_clover_products.clover_auth_id = '" . $merchant['id'] . "' AND tbl_clover_products_stock.status = 'UPDATE CLOVER STOCK TRUE'";
    $products = $db->fetchResult($sql);
    foreach ($products as $product) {
        $response = $CloverAPI->updateStock($product['clover_id'], $product['stock_count']);
        if (!empty($response['modifiedTime'])) {
            echo "Store id- " . $merchant['store_id'] . "\n";
            echo "Location id - " . $merchant['location_id'] . "\n";
            echo "Merchant id - " . $merchant['w3b_merchant_id'] . "\n";
            $db->executeQuery("UPDATE tbl_clover_products_stock SET status='UPDATE STOCK FALSE' WHERE id = '" . $product['stock_id'] . "'");
        } else {
            $w3bstore->logApiError($merchant['id'], 'clover-setqty', $product, $response, '');
        }
    }
}
echo "End clover stock\n\n";
