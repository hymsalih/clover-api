<?php
/**
 * Created by PhpStorm.
 * User: Salih Mohamed
 * Date: 7/31/2019
 * Time: 11:40 AM
 */
set_time_limit(0);
include __DIR__ . "/db/Database.php";
$db = new Database();
include './CloverAPI.php';
include './W3bstoreAPI.php';
include './vendor/autoload.php';
require_once(__DIR__ . '/vendor/autoload.php');
if (file_exists('./env.php')) {
    include './env.php';
}
$CloverAPI = new W3bStore\CloverAPI();
$merchants = $db->fetchResult("SELECT * from tbl_clover_authentications");
foreach ($merchants as $merchant) {
    $customers = $CloverAPI->readAllCustomers();
    if (!isset($customers->elements))
        continue;

    echo count($customers->elements) . "\n";
    foreach ($customers->elements as $p_key => $customer) {
        $response = $CloverAPI->deleteCustomer($customer->id);
        print_r($response);
    }
}
echo "\n\nDone!";
