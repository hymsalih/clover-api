<?php

namespace W3bStore;
class CloverAPI
{
    public function cloverApi($endPoint, $postData, $method, $token, $queryParams = null)
    {
        global $ENV;
        $curl = curl_init();
        if ($method == 'GET') {
            curl_setopt_array($curl, array(
                CURLOPT_URL => $ENV['BASE_URL'] . $endPoint,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_POSTFIELDS => "",
                CURLOPT_HTTPHEADER => array(
                    "Authorization: Bearer $token",
                    "content-type: application/json"
                ),
            ));
            $response = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);
            if ($err) {
                echo "cURL Error #:" . $err;
                exit;
            } else {
                return $response;
            }
        } else if ($method == 'POST') {
            curl_setopt_array($curl, array(
                CURLOPT_URL => $ENV['BASE_URL'] . $endPoint,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => json_encode($postData),
                CURLOPT_HTTPHEADER => array(
                    "Authorization: Bearer $token",
                    "content-type: application/json"
                ),
            ));
            $response = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);
            if ($err) {
                echo "cURL Error #:" . $err;
                exit;
            } else {
                return $response;
            }
        } else if ($method == 'DELETE') {
            curl_setopt_array($curl, array(
                CURLOPT_URL => $ENV['BASE_URL'] . $endPoint,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "DELETE",
//                CURLOPT_POSTFIELDS => json_encode($postData),
                CURLOPT_HTTPHEADER => array(
                    "Authorization: Bearer $token",
                    "content-type: application/json"
                ),
            ));
            $response = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);
            if ($err) {
                echo "cURL Error #:" . $err;
                exit;
            } else {
                return $response;
            }
        } else if ($method == 'PUT') {
            curl_setopt_array($curl, array(
                CURLOPT_URL => $ENV['BASE_URL'] . $endPoint,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "PUT",
                CURLOPT_POSTFIELDS => json_encode($postData),
                CURLOPT_HTTPHEADER => array(
                    "Authorization: Bearer $token",
                    "content-type: application/json"
                ),
            ));
            $response = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);
            if ($err) {
                echo "cURL Error #:" . $err;
                exit;
            } else {
                return $response;
            }
        }

    }

    public function OAuthURL()
    {
        global $ENV;
        return $ENV['AUTH_URL'] . $ENV['CLIENT_ID'];
    }

    public function deleteCustomer($customerId)
    {
        global $merchant;
        $cloverAPI = new CloverAPI();
        $response = $cloverAPI->cloverApi($merchant['merchant_id'] . "/customers/" . $customerId, [], 'DELETE', $merchant['token'], []);
        return json_decode($response);
    }

    public function deleteOrder($orderId)
    {
        global $merchant;
        $cloverAPI = new CloverAPI();
        $response = $cloverAPI->cloverApi($merchant['merchant_id'] . "/orders/" . $orderId, [], 'DELETE', $merchant['token'], []);
        return json_decode($response);
    }


    public function readAllCustomers()
    {
        global $merchant;
        $cloverAPI = new CloverAPI();
        $response = $cloverAPI->cloverApi($merchant['merchant_id'] . "/customers/", [], 'GET', $merchant['token'], []);
        return json_decode($response);
    }

    public function readCustomer($customerId, $parms)
    {
        global $merchant;
        $cloverAPI = new CloverAPI();
        $parms = "expand=" . implode(",", $parms);
        $response = $cloverAPI->cloverApi($merchant['merchant_id'] . "/customers/" . $customerId . "?" . $parms, [], 'GET', $merchant['token'], []);
        return json_decode($response);
    }

    public function addNewCustomer($customerDetails)
    {
        global $merchant;
        $cloverAPI = new CloverAPI();
        $response = $cloverAPI->cloverApi($merchant['merchant_id'] . "/customers", $customerDetails, 'POST', $merchant['token'], []);
        return json_decode($response);
    }

    function readOrders($merchant, $filters = [], $parms = [])
    {
        $cloverAPI = new CloverAPI();
        $filters = "expand=" . implode(",", $filters);
        $parms = http_build_query($parms);
        $response = $cloverAPI->cloverApi($merchant['merchant_id'] . "/orders?" . $filters . "&" . $parms, null, 'GET', $merchant['token'], []);
        return json_decode($response);
    }
    function readProducts($merchant, $filters = [], $parms = [])
    {
        $cloverAPI = new CloverAPI();
        $filters = "expand=" . implode(",", $filters);
        $parms = http_build_query($parms);
        $response = $cloverAPI->cloverApi($merchant['merchant_id'] . "/items?" . $filters . "&" . $parms, null, 'GET', $merchant['token'], []);
        return json_decode($response);
    }

    function getProduct($itemId, $filters)
    {
        global $merchant;
        $cloverAPI = new CloverAPI();
        $filters = "expand=" . implode(",", $filters);
        $response = $cloverAPI->cloverApi($merchant['merchant_id'] . "/items/$itemId?" . $filters, null, 'GET', $merchant['token'], []);
        return json_decode($response);
    }

    function readAttributes($merchant)
    {
        $cloverAPI = new CloverAPI();
        $response = $cloverAPI->cloverApi($merchant['merchant_id'] . "/attributes", null, 'GET', $merchant['token'], []);
        return json_decode($response);
    }

    function readAttributesById($merchant, $attributeId)
    {
        $cloverAPI = new CloverAPI();
        $response = $cloverAPI->cloverApi($merchant['merchant_id'] . "/attributes/" . $attributeId, null, 'GET', $merchant['token'], []);
        return json_decode($response);
    }

    function addOrderLineItems($merchant, $orderId, $postData)
    {
        $cloverAPI = new CloverAPI();
//        $response = $cloverAPI->cloverApi($merchant['merchant_id'] . "/orders/" . $orderId . "/line_items", $postData, 'POST', $merchant['token'], []);
        $response = $cloverAPI->cloverApi($merchant['merchant_id'] . "/orders/" . $orderId . "/bulk_line_items", $postData, 'POST', $merchant['token'], []);
        return json_decode($response);
    }

    function addOrderLineItemsBulk($merchant, $orderId, $postData)
    {
        $cloverAPI = new CloverAPI();
        $response = $cloverAPI->cloverApi($merchant['merchant_id'] . "/orders/" . $orderId . "/bulk_line_items", $postData, 'POST', $merchant['token'], []);
        return json_decode($response);
    }

    function addOrders($merchant, $postData)
    {
        $cloverAPI = new CloverAPI();
        $response = $cloverAPI->cloverApi($merchant['merchant_id'] . "/orders", $postData, 'POST', $merchant['token'], []);
        return json_decode($response);
    }

    function updateOrder($merchant, $orderId, $postData)
    {
        $cloverAPI = new CloverAPI();
        $response = $cloverAPI->cloverApi($merchant['merchant_id'] . "/orders/" . $orderId, $postData, 'POST', $merchant['token'], []);
        return json_decode($response);
    }

    function updateModifierGroup($groupId, $data)
    {
        global $merchant;
        $cloverAPI = new CloverAPI();
        $response = $cloverAPI->cloverApi($merchant['merchant_id'] . "/modifier_groups/$groupId", $data, 'PUT', $merchant['token'], []);
        return json_decode($response);
    }

    function readModifierGroup()
    {
        global $merchant;
        $cloverAPI = new CloverAPI();
        $response = $cloverAPI->cloverApi($merchant['merchant_id'] . "/modifier_groups?expand=modifiers", null, 'GET', $merchant['token'], []);
        return json_decode($response);
    }

    function readModifier($merchant, $modGroupId)
    {
        global $merchant;
        $cloverAPI = new CloverAPI();
        $response = $cloverAPI->cloverApi($merchant['merchant_id'] . "/modifier_groups/$modGroupId/modifiers", null, 'GET', $merchant['token'], []);
        return json_decode($response);
    }

    function readItemGroup($itemGroupsId = null)
    {
        global $merchant;
        $cloverAPI = new CloverAPI();
        if (empty($itemGroupsId))
            $response = $cloverAPI->cloverApi($merchant['merchant_id'] . "/item_groups", null, 'GET', $merchant['token'], ['expand' => 'attributes']);
        else
            $response = $cloverAPI->cloverApi($merchant['merchant_id'] . "/item_groups/" . $itemGroupsId . "?expand=attributes,items", null, 'GET', $merchant['token'], ['expand' => 'attributes']);
        return json_decode($response);
    }

    function deleteData($type)
    {
        global $merchant;
        $cloverAPI = new CloverAPI();
        $response = $cloverAPI->cloverApi($merchant['merchant_id'] . $type, null, 'DELETE', $merchant['token'], []);
        return json_decode($response);
    }

    function readAllOptions()
    {
        global $merchant;
        $cloverAPI = new CloverAPI();
        $response = $cloverAPI->cloverApi($merchant['merchant_id'] . "/options", null, 'GET', $merchant['token'], []);
        return json_decode($response);
    }

    function readProductOption($attributeId)
    {
        global $merchant;
        $cloverAPI = new CloverAPI();
        $response = $cloverAPI->cloverApi($merchant['merchant_id'] . "/attributes/" . $attributeId . "/options", null, 'GET', $merchant['token'], []);
        return json_decode($response);
    }

    function createNewCategory($name, $sortOrder)
    {
        global $merchant;
        $newCategory = [
            'name' => $name,
            'sortOrder' => $sortOrder
        ];
        $cloverAPI = new CloverAPI();
        $response = $cloverAPI->cloverApi($merchant['merchant_id'] . "/categories/", $newCategory, 'POST', $merchant['token'], []);
        return json_decode($response);
    }


    function createNewTags($name)
    {
        global $merchant;
        $newTags = [
            'name' => $name
        ];
        $cloverAPI = new CloverAPI();
        $response = $cloverAPI->cloverApi($merchant['merchant_id'] . "/tags/", $newTags, 'POST', $merchant['token'], []);
        return json_decode($response);
    }

    function createTaxRates($name, $isDefault, $rate, $taxAmount, $taxType)
    {
        global $merchant;
        $newTaxRates = [
            'name' => $name,
            'isDefault' => $isDefault,
            'rate' => $rate,
            'taxAmount' => $taxAmount,
            'taxType' => $taxType,//VAT_TAXABLE,VAT_NON_TAXABLE,VAT_EXEMPT,INTERNAL_TAX

        ];
        $cloverAPI = new CloverAPI();
        $response = $cloverAPI->cloverApi($merchant['merchant_id'] . "/tax_rates/", $newTaxRates, 'POST', $merchant['token'], []);
        return json_decode($response);
    }

    function createModifierGroup($postData)
    {
        global $merchant;
        $cloverAPI = new CloverAPI();
        $response = $cloverAPI->cloverApi($merchant['merchant_id'] . "/modifier_groups/", $postData, 'POST', $merchant['token'], []);
        return json_decode($response, true);
    }

    function createModifier($modifierGroupId, $name, $price)
    {
        global $merchant;
        $modifier = [
            'name' => $name,
            'price' => (int)$price,

        ];
        $cloverAPI = new CloverAPI();
        $response = $cloverAPI->cloverApi($merchant['merchant_id'] . "/modifier_groups/" . $modifierGroupId . "/modifiers", $modifier, 'POST', $merchant['token'], []);
        return json_decode($response, true);
    }

    function updateProductPrices($productId, $newPrice, $newCost)
    {
        global $merchant;
        $productDetails = ['price' => $newPrice, 'cost' => $newCost];
        $cloverAPI = new CloverAPI();
        $response = $cloverAPI->cloverApi($merchant['merchant_id'] . "/items/$productId", $productDetails, 'POST', $merchant['token'], []);
        return json_decode($response, true);
    }

    function updateStock($productId, $quantity)
    {
        global $merchant;
        $productDetails = ['quantity' => $quantity];
        $cloverAPI = new CloverAPI();
        $response = $cloverAPI->cloverApi($merchant['merchant_id'] . "/item_stocks/$productId", $productDetails, 'POST', $merchant['token'], []);
        return json_decode($response, true);
    }

    function createNewProduct($productDetails)
    {
        global $merchant;
        $cloverAPI = new CloverAPI();
        $response = $cloverAPI->cloverApi($merchant['merchant_id'] . "/items/", $productDetails, 'POST', $merchant['token'], []);
        return json_decode($response, true);
    }


    function createNewGroup($groupName)
    {
        global $merchant;
        $cloverAPI = new CloverAPI();
        $response = $cloverAPI->cloverApi($merchant['merchant_id'] . "/item_groups/", ['name' => $groupName], 'POST', $merchant['token'], []);
        return json_decode($response, true);
    }

    function createNewAtributes($attributeName, $itemGroupId)
    {
        global $merchant;
        $cloverAPI = new CloverAPI();
        $response = $cloverAPI->cloverApi($merchant['merchant_id'] . "/attributes/", ['name' => $attributeName, 'itemGroup' => ['id' => $itemGroupId]], 'POST', $merchant['token'], []);
        return json_decode($response, true);
    }

    function createNewOptions($optionName, $attributeId)
    {
        global $merchant;
        $cloverAPI = new CloverAPI();
        $response = $cloverAPI->cloverApi($merchant['merchant_id'] . "/attributes/$attributeId/options", ['name' => $optionName], 'POST', $merchant['token'], []);
        return json_decode($response, true);
    }

    function createNewOptionItems($itemId, $optionId)
    {
        $requestBody = [
            'elements' => [
                [
                    'item' => ['id' => $itemId],
                    'option' => ['id' => $optionId],
                ]
            ]
        ];
        global $merchant;
        $cloverAPI = new CloverAPI();
        $response = $cloverAPI->cloverApi($merchant['merchant_id'] . "/option_items", $requestBody, 'POST', $merchant['token'], []);
        return json_decode($response);
    }

    function getProductQuantity($parms)
    {
        global $merchant;
        $cloverAPI = new CloverAPI();
        $parms = http_build_query($parms);
        $response = $cloverAPI->cloverApi($merchant['merchant_id'] . "/items?" . $filters . "&" . $parms, null, 'GET', $merchant['token'], []);
        return json_decode($response);
    }

    function getAllGroupName()
    {
        global $merchant;
        $cloverAPI = new CloverAPI();
        $response = $cloverAPI->cloverApi($merchant['merchant_id'] . "/item_groups/", null, 'GET', $merchant['token'], []);
        return json_decode($response);
    }

    function getPaymentStatus($status)
    {
        $order_status = ['1' => '', '2' => '', '3' => ''];
        if (array_key_exists($status, $order_status))
            return $order_status[$status];
        else
            return "";
    }

    function getOrderStatus($status)
    {
        $order_status = ['1' => '', '2' => '', '3' => ''];
        if (array_key_exists($status, $order_status))
            return $order_status[$status];
        else
            return "";
    }

    function getPayType($payType)
    {
        $payTypes = ['1' => 'SPLITGUEST', '2' => 'SPLIT_ITEM', '3' => 'SPLIT_CUSTOM', '4' => 'FULL'];
        if (array_key_exists($payType, $payTypes))
            return $payTypes[$payType];
        else
            return "";
    }


}
