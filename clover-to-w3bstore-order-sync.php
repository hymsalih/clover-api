<?php
/**
 * Created by PhpStorm.
 * User: Salih Mohamed
 * Date: 7/31/2019
 * Time: 11:40 AM
 */
set_time_limit(0);
include __DIR__ . "/db/Database.php";
$db = new Database();
include './CloverAPI.php';
include './W3bstoreAPI.php';
include './vendor/autoload.php';
require_once(__DIR__ . '/vendor/autoload.php');
if (file_exists('./env.php')) {
    include './env.php';
}
global $ENV;
$CloverAPI = new W3bStore\CloverAPI();
$merchants = $db->fetchResult("SELECT * from tbl_clover_authentications WHERE location_id IS NOT NULL AND store_id IS NOT NULL AND token IS NOT NULL AND w3b_merchant_id IS NOT NULL AND merchant_id IS NOT NULL AND w3b_api_key IS NOT NULL");
foreach ($merchants as $merchant) {
    $location = $db->fetchResult("SELECT * from tbl_member_store_shipping_suppliers WHERE id = '" . $merchant['location_id'] . "'");
    if (empty($location)) {
        echo "Invalid location name\n";
        continue;
    }
    $locationName = $location[0]['warehouse_supplier_name'];
    $locationId = $location[0]['id'];
    $orders = $CloverAPI->readOrders($merchant, ['lineItems', 'serviceCharge', 'discounts', 'credits', 'payments', 'refunds'], ['limit' => 1000]);
    if (!isset($orders->elements))
        continue;

//    $lastSync = $db->fetchResult("SELECT * FROM tbl_ecommerce_sync_history WHERE 	source = 'Clover' AND auth_id = '" . $merchant['id'] . "' ");
//    $lastDateTime = strtotime(date("Y-m-d h:i:s", strtotime("yesterday")));
//    if (!empty($lastSync)) {
//        $lastDateTime = strtotime($lastSync[0]['last_sync_date']);
//    }
    $W3bstoreAPI = new W3bStore\W3bstoreAPI($merchant['store_id'], $merchant['w3b_merchant_id'], $merchant['w3b_api_key']);
    foreach ($orders->elements as $p_key => $order) {
        $sql = "SELECT * FROM `tbl_clover_orders` where clover_id = '" . $order->id . "' AND clover_auth_id='" . $merchant['id'] . "'";
        $orderRec = $db->fetchResult($sql);
        if (empty($orderRec)) {
            $items = [];
            $totalQty = 0;
            if (isset($order->lineItems->elements)) {
                foreach ($order->lineItems->elements as $item) {
                    $sql = "SELECT * FROM tbl_clover_products WHERE member_item_id IS NOT NULL AND clover_id = '" . $item->item->id . "' AND clover_auth_id = '" . $merchant['id'] . "'";
                    $product = $db->fetchResult($sql);
                    if (empty($product))
                        continue;

                    if (!isset($item->unitQty))
                        $item->unitQty = 1000;

                    $totalQty = $totalQty + ($item->unitQty / 1000);
                    $items[] = [
                        'item_id' => $product[0]['member_item_id'],
                        'item_title' => $item->name,
                        'variant_group_name' => 'N/A',
                        'qty' => ($item->unitQty / 1000),
                        'qty_fulfilled' => ($item->unitQty / 1000),
                        'offer_price' => ($item->price / 100),
                        'discounted_price' => 0,
                        'qty_discount' => NULL,
                        'qty_discount_type' => NULL,
                        'bundle_id' => NULL,
                        'bundle_discount' => NULL,
                        'bundle_discount_type' => NULL,
                        'tax' => '0',
                        'tax_region' => NULL,
                        'tax_county' => '0.00',
                        'tax_city' => '0.00',
                        'tax_special' => '0.00',
                        'tax_shipping' => NULL,
                        'region_shipping_origin' => 'Florida',
                        'zip_shipping_origin' => NULL,
                        'status' => '0',
                        'notes' => NULL,
                        'reviewed' => NULL,
                        'qty_captured' => NULL,
                        'shipping_info_id' => $locationId,
                        'cog' => '0.00',
                        'qty_refunded' => NULL,
                        'last_qty_refunded' => NULL,
                        'comission' => NULL,
                        'acc_for' => NULL,
                        'special_instruction' => '0',
                        'order_iteraction' => '1',
                        'warehouse_bin' => '',
                        'location' => $locationName,
                    ];
                }

                if (count($order->lineItems->elements) != count($items)) {
                    $w3bstore->logApiError($merchant['id'], '', '', '', 'Clover order ' . $order->id . ' ordered items and w3bstore items are not matched');
                    echo "Clover ordered items and w3bstore items are not matched\n";
                    continue;
                }
            }
            $customerId = "";
            if (!empty($order->customers)) {
                $cloverCustomerDetails = $order->customers->elements[0];
                $sql = "SELECT * FROM `tbl_clover_customers` where clover_id = '" . $order->id . "' AND clover_auth_id='" . $merchant['id'] . "'";
                $hasCustomer = $db->fetchResult($sql);
                if (empty($hasCustomer)) {
                    $cloverCustomerDetails = $CloverAPI->readCustomer($cloverCustomerDetails->id, ['addresses', 'emailAddresses', 'phoneNumbers', 'cards', 'metadata']);
                    if (!empty($cloverCustomerDetails->id)) {
                        $countryCode = "";
                        if (isset($cloverCustomerDetails->addresses->elements[0]->country)) {
                            $countryCode = $db->fetchResult("SELECT * FROM tbl_country WHERE iso = '" . strtoupper($cloverCustomerDetails->addresses->elements[0]->country) . "'");
                            $countryCode = $countryCode[0]['id'];
                        }
                        $customerEmail = (isset($cloverCustomerDetails->emailAddresses->elements[0]->emailAddress) ? $cloverCustomerDetails->emailAddresses->elements[0]->emailAddress : '');
                        $customerEmail = strtolower($customerEmail);
                        $customerRequestBody[] = [
                            'email' => $customerEmail,
                            'password' => $cloverCustomerDetails->lastName . (isset($cloverCustomerDetails->addresses->elements[0]->zip) ? $cloverCustomerDetails->addresses->elements[0]->zip : ''),
                            'first_name' => $cloverCustomerDetails->firstName,
                            'last_name' => $cloverCustomerDetails->lastName,
                            'phone' => (isset($cloverCustomerDetails->phoneNumbers->elements[0]->phoneNumber) ? $cloverCustomerDetails->phoneNumbers->elements[0]->phoneNumber : ''),
                            'country' => $countryCode,
                            'state' => (isset($cloverCustomerDetails->addresses->elements[0]->state) ? $cloverCustomerDetails->addresses->elements[0]->state : ''),
                            'zip' => (isset($cloverCustomerDetails->addresses->elements[0]->zip) ? $cloverCustomerDetails->addresses->elements[0]->zip : ''),
                            'city' => (isset($cloverCustomerDetails->addresses->elements[0]->city) ? $cloverCustomerDetails->addresses->elements[0]->city : ''),
                            'street' => (isset($cloverCustomerDetails->addresses->elements[0]->address1) ? $cloverCustomerDetails->addresses->elements[0]->address1 : ''),
                            'street2' => (isset($cloverCustomerDetails->addresses->elements[0]->address2) ? $cloverCustomerDetails->addresses->elements[0]->address2 : ''),
                            'created_at' => date("Y-m-d H:i:s", substr($cloverCustomerDetails->customerSince, 0, 10)),
                            'updated_at' => date("Y-m-d H:i:s", substr($cloverCustomerDetails->customerSince, 0, 10)),
                            'ein' => '',
                            'landline' => '',
                            'notes' => (isset($cloverCustomerDetails->metadata->note) ? $cloverCustomerDetails->metadata->note : ''),
                            'appointent' => NULL,
                            'contact_preference' => 0,
                            'carrier' => NULL,
                            'balance' => 0,
                            'store_credit_returned' => NULL,
                            'store_credit_redeemed' => NULL,
                            'store_credit_last_redemption' => NULL,
                            'loyalty_earned' => 0,
                            'loyalty_last_redemption' => NULL,
                            'discount' => NULL,
                            'loyalty_factor' => NULL,
                        ];
                        $customerResponse = $W3bstoreAPI->request($ENV['W3BSTORE_API'], 'POST', json_encode($customerRequestBody), 'add_customers');
                        if ($customerResponse['error'] == 0) {
                            try {
                                $customerId = $customerResponse['response'][0][$customerEmail];
                                $hasCustomer = $db->fetchResult("SELECT * FROM tbl_clover_customers WHERE clover_id = '" . $cloverCustomerDetails->id . "' AND customer_id = '$customerId' AND clover_auth_id = '" . $merchant['id'] . "' ");
                                if (empty($hasCustomer)) {
                                    $db->executeQuery("INSERT INTO `tbl_clover_customers` (`id`, `clover_auth_id`, `clover_id`, `customer_id`, `first_name`, `last_name`, `customer_since`) VALUES (NULL, '" . $merchant['id'] . "',
                                     '" . $cloverCustomerDetails->id . "', '" . $customerId . "', '" . $cloverCustomerDetails->firstName . "', '" . $cloverCustomerDetails->lastName . "', '" . date('Y-m-d h:i:s', $cloverCustomerDetails->customerSince) . "')");
                                }
                            } catch (Exception $e) {
                                $W3bstoreAPI->logApiError($merchant['id'], 'w3bstore-add_customers', $customerRequestBody, $customerResponse, $e->getMessage());
                                continue;
                            }
                        } else {
                            $pos = strpos($customerResponse['msg'], 'already exists');
                            if ($pos !== false) {
                                $customerResponse = $W3bstoreAPI->request($ENV['W3BSTORE_API'], 'GET', json_encode($customerRequestBody), 'get_customers', ['email' => $customerEmail]);
                                if (!empty($customerResponse)) {
                                    $customerId = $customerResponse['response'][0]['id'];
                                    $db->executeQuery("INSERT INTO `tbl_clover_customers` (`id`, `clover_auth_id`, `clover_id`, `customer_id`, `first_name`, `last_name`, `customer_since`) VALUES (NULL, '" . $merchant['id'] . "',
                                     '" . $cloverCustomerDetails->id . "', '" . $customerId . "', '" . $cloverCustomerDetails->firstName . "', '" . $cloverCustomerDetails->lastName . "', '" . date("Y-m-d H:i:s", substr($cloverCustomerDetails->customerSince, 0, 10)) . "')");
                                }
                            }
                        }
                    }
                }
            }
            $w3bstoreOrders[] = [
                'customer_id' => $customerId,
                'sub_total_price' => ($order->total / 100),
                'calculated_discount' => 0,
                'tax' => 0,
                'shipping_handling_cost' => 0,
                'total_price' => ($order->total / 100),
                'total_captured' => 0,
                'payment_id' => 'invoice',
                'payment_status' => 10,
                'order_date' => date("Y-m-d H:i:s", substr($order->createdTime, 0, 10)),
                'order_status' => 17,
                'notes' => NULL,
                'last_action_date' => date("Y-m-d H:i:s", substr($order->modifiedTime, 0, 10)),
                'invoice_notes' => NULL,
                'promo_code' => NULL,
                'created_at' => date("Y-m-d H:i:s", substr($order->createdTime, 0, 10)),
                'updated_at' => date("Y-m-d H:i:s", substr($order->modifiedTime, 0, 10)),
                'ip_address' => NULL,
                'avs_failed' => NULL,
                'referal' => '',
                'pickup_time' => date("Y-m-d H:i:s", substr($order->createdTime, 0, 10)),
                'tip' => 0,
                'credit' => 0,
                'returns' => 0,
                'change_val' => NULL,
                'terms' => NULL,
                'iteration' => 1,
                'refunds' => NULL,
                'tax_region' => 0,
                'tax_county' => 0,
                'tax_city' => 0,
                'tax_special' => 0,
                'tax_shipping' => 0,
                'cc_bin' => NULL,
                'cc_account' => NULL,
                'cc_type' => NULL,
                'items' => $items,
                'history' => []
            ];
            $orderResponse = $W3bstoreAPI->request($ENV['W3BSTORE_API'], 'POST', json_encode($w3bstoreOrders), 'add_orders');
            if ($orderResponse['error'] == 0) {
                if (!empty($orderResponse['response']) && $orderResponse['response']['result'] == 'OK') {
                    $w3bstoreOrderId = $orderResponse['response']['order_id'][0]['id'];
                    $sql = "INSERT INTO `tbl_clover_orders` ( `clover_auth_id`, `clover_id`, `currency`, `customer_id`,`total`,`tax_removed`,`is_vat`, `state`, `manual_transaction`, `group_line_items`, `test_mode`, `created_time`, `client_created_time`, `modified_time`, `order_id`,`total_qty`) VALUES ('" . $merchant['id'] . "','" . $order->id . "', '" . (isset($order->currency) ? $order->currency : '') . "', '" . $customerId . "', '" . (isset($order->total) ? $order->total : 0) . "', '" . (isset($order->taxRemoved) ? $order->taxRemoved : '') . "', '" . (isset($order->isVat) ? $order->isVat : '') . "', '" . (isset($order->state) ? $order->state : '') . "', '" . (isset($order->manualTransaction) ? $order->manualTransaction : '') . "', '" . (isset($order->groupLineItems) ? $order->groupLineItems : '') . "', '" . (isset($order->testMode) ? $order->testMode : '') . "', '" . $order->createdTime . "',  '" . $order->clientCreatedTime . "', '" . $order->modifiedTime . "', '" . $w3bstoreOrderId . "', '" . $totalQty . "')";
                    $db->executeQuery($sql);
                    $orderId = $db->lastInsetedId();
                }
            }
        } else {
            $orderId = $orderRec[0]['id'];
        }
        echo "Order------------------" . $orderId . " - " . $order->id . "\n";
    }
}
echo "\n\nDone!";
