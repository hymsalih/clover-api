<?php
/**
 * Created by PhpStorm.
 * User: Salih Mohamed
 * Date: 8/16/2019
 * Time: 1:23 AM
 */
session_start();
session_destroy();
header("Location: login.php");
exit;