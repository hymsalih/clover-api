<?php
/**
 * Created by PhpStorm.
 * User: Salih Mohamed
 * Date: 8/20/2019
 * Time: 9:01 PM
 */
set_time_limit(0);
include __DIR__ . "/db/Database.php";
$db = new Database();
include './CloverAPI.php';
include './vendor/autoload.php';
if (file_exists('./env.php')) {
    include './env.php';
}
$merchants = $db->fetchResult("SELECT * from tbl_clover_authentications");
$cloverProducts = [];
foreach ($merchants as $merchant) {
    $CloverAPI = new W3bStore\CloverAPI();
    $options = $CloverAPI->readItemGroup();
    foreach ($options->elements as $option) {
        $response = $CloverAPI->deleteData($type = '/item_groups/' . $option->id);
    }
    $CloverAPI = new W3bStore\CloverAPI();
    $optionIds = [];
    $attributeIds = [];
    foreach ($options->elements as $option) {
        $optionIds[] = $option->id;
        if (isset($option->attribute)) {
            $attributeIds[] = $option->attribute->id;
        }
    }

    $CloverAPI = new W3bStore\CloverAPI();
    $options = $CloverAPI->readAllOptions();
    $optionIds = [];
    $attributeIds = [];
    foreach ($options->elements as $option) {
        $optionIds[] = $option->id;
        if (isset($option->attribute)) {
            $attributeIds[] = $option->attribute->id;
        }
    }
    $attributeIds = array_unique($attributeIds);
    foreach ($attributeIds as $attributeId) {
        $response = $CloverAPI->deleteData($type = '/attributes/' . $attributeId);
    }
}
